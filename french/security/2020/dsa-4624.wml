#use wml::debian::translation-check translation="8be684d647389ee3db99d941206fa9b5cbef2621" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans evince, un
visualisateur simple de documents multipages.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000159">CVE-2017-1000159</a>

<p>Tobias Mueller a signalé que l'exportateur DVI dans evince est sujet à
une vulnérabilité d'injection de commande au moyen de noms de fichier
contrefaits pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>

<p>Andy Nguyen a signalé que les fonctions tiff_document_render() et
tiff_document_get_thumbnail() dans le moteur de documents TIFF ne gérait
pas les erreurs provenant de TIFFReadRGBAImageOriented(), menant à la
divulgation de mémoire non initialisée lors du traitement de fichiers
d'images TIFF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010006">CVE-2019-1010006</a>

<p>Une vulnérabilité de dépassement de tampon dans le moteur tiff pourrait
conduire à un déni de service, ou éventuellement à l'exécution de code
arbitraire lors de l'ouverture d'un fichier PDF contrefait pour l'occasion.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 3.22.1-3+deb9u2.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 3.30.2-3+deb10u1. La distribution stable n'est affectée que
par le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>.</p>

<p>Nous vous recommandons de mettre à jour vos paquets evince.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de evince, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/evince">\
https://security-tracker.debian.org/tracker/evince</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4624.data"
# $Id: $
