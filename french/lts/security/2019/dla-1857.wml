#use wml::debian::translation-check translation="b01b1c098a6fbb195a93b58e9165fe7f14a04d05" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Des vulnérabilités ont été découvertes dans nss, la bibliothèque du
service Network Security Service de Mozilla.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11719">CVE-2019-11719</a>

<p>Lecture hors limites lors de l’import de la clef privée curve25519</p>

<p>Lors de l’import d’une clef privée curve25519 au format PKCS#8 avec des
octets 0x00 au début, il est possible de déclencher une lecture hors limites
dans la bibliothèque NSS (Network Security Service). Cela pourrait conduire à
une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11729">CVE-2019-11729</a>

<p>Clefs publiques vides ou mal formées 256-ECDH pouvant déclencher une erreur
de segmentation</p>

<p>Des clefs publiques vides ou mal formées 256-ECDH pouvent déclencher une
erreur de segmentation à cause de valeurs improprement vérifiées avant d’être
copiées en mémoire et utilisées.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2:3.26-1+debu8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1857.data"
# $Id: $
