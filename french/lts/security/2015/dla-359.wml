#use wml::debian::translation-check translation="04d941142caea6346972828d64c63b95b571b8f1" maintainer="Jean-Pierre Giraud"
<define-tag description>Ajout des paquets MySQL 5.5 ; fin de prise en charge pour MySQL 5.1</define-tag>
<define-tag moreinfo>
<p>Oracle, le responsable amont de MySQL, ne prend plus en compte la
version 5.1 de MySQL, qui est incluse dans Debian 6.0 <q>Squeeze</q>.
Vraisemblablement MySQL 5.1 souffre de multiples vulnérabilités corrigées
dans des versions plus récentes après la fin de la prise en charge amont,
mais Oracle ne divulgue pas suffisamment d'informations pour vérifier ou
corriger ces vulnérabilités.</p>

<p>Comme alternative, l'équipe Debian LTS fournit des paquets MySQL 5.5 à
utiliser dans Debian 6.0 <q>Squeeze</q>. Nous recommandons aux utilisateurs
de Squeeze LTS de les installer et de migrer leurs bases de données.</p>

<p>Veuillez noter qu'un « dist-upgrade » ne tiendra pas compte de ces
paquets MySQL 5.5 automatiquement, donc les utilisateurs doivent les
installer explicitement.</p>

<p>Si vous utilisez un serveur MySQL :</p>

<pre>apt-get install mysql-server-5.5</pre>

<p>Si vous voulez seulement le client MySQL :</p>

<pre>apt-get install mysql-client-5.5</pre>


<h3>Mises à jour pour compatibilité</h3>

<p>Certains paquets ont été mis à jour pour résoudre des problèmes
d'incompatibilité. Ils ont été corrigés dans les versions suivantes :</p>

<ul>
<li>bacula-director-mysql 5.0.2-2.2+squeeze2</li>
<li>cacti 0.8.7g-1+squeeze9</li>
<li>phpmyadmin 4:3.3.7-10</li>
<li>postfix-policyd 1.82-2+deb6u1</li>
<li>prelude-manager 1.0.0-1+deb6u1</li>
</ul>

<p>Nous vous recommandons de mettre à niveau ces paquets avant la mise à
niveau de MySQL 5.5. Un simple « dist-upgrade » devrait suffire.</p>

<p>Nous avons fait au mieux pour fournir des paquets MySQL 5.5 fiables. Des
paquets de test ont été disponibles pendant un certain temps, mais nous
n'avons pas reçu de retour des utilisateurs. En tout cas, n'hésitez pas à
rapporter tout problème lié à cette mise à niveau de MySQL à
debian-lts@lists.debian.org.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2015/dla-359.data"
# $Id: $
