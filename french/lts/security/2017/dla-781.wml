#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Asterisk, une boîte
à outils au code source ouvert pour PBX et de la téléphonie.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2287">CVE-2014-2287</a>

<p>channels/chan_sip.c dans Asterisk, lorsque chan_sip a une certaine
configuration, permet à des utilisateurs distants authentifiés de provoquer
un déni de service (consommation de canaux et de descripteurs de fichier) à
l'aide d'une requête INVITE avec un en-tête (1) Session-Expires ou (2) Min-SE
ayant une valeur mal formée ou non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7551">CVE-2016-7551</a>

<p>La fonctionnalité de composition temporisée (overlap dialing) dans chan_sip
permet à chan_sip de signaler à un appareil que le numéro qui a été composé est
incomplet et que plus de numéros sont nécessaires. Si cette fonctionnalité est
utilisée avec un appareil qui a réalisé une authentification
nom d'utilisateur/mot de passe, des ressources RTP sont divulguées. Cela survient
parce que le code échoue à libérer les anciennes ressources RTP avant d'en allouer
de nouvelles dans ce scénario. Si toutes les ressources sont utilisées, alors
un épuisement des ports RTP surviendra et aucune session RTP ne pourra être
établie.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:1.8.13.1~dfsg1-3+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-781.data"
# $Id: $
