#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de contrefaçon de requête coté serveur (SSRF) a été
signalée pour le script de configuration dans phpmyadmin, un outil
d’administration pour MYSQL. Ce défaut pouvait permettre à un attaquant non
authentifié de retrouver par force brute les mots de passe pour MYSQL, de
détecter les noms d’hôte internes et les ports ouverts sur le réseau interne. De
plus, il existait une situation de compétition entre l’écriture de configuration
et l’administrateur la modifiant, permettant à un utilisateur non authentifié de
le lire ou de le changer. Les utilisateurs de Debian qui configurent phpmyadmin à
l’aide de debconf et utilisent la configuration par défaut pour Apache 2 ou
Lighttpd n’ont jamais été affectés.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4:3.4.11.1-2+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpmyadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-834.data"
# $Id: $
