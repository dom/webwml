#use wml::debian::translation-check translation="f06501c0d28e56630b8ef9e96e180d00ebd26066" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs bogues CVE concernant src:netqmail ont été signalés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2005-1513">CVE-2005-1513</a>

<p>Un dépassement d'entier dans la fonction stralloc_readyplus dans qmail, lors
d’une exécution sur une plateforme 64 bits avec une grande utilisation de
mémoire virtuelle, permet à des attaquants distants de provoquer un déni de
service et, éventuellement, d’exécuter du code arbitraire à l'aide d'une requête
SMTP importante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2005-1514">CVE-2005-1514</a>

<p>La fonction commands.c dans qmail, lors d’une exécution sur une plateforme
64 bits avec une grande utilisation de mémoire virtuelle, permet à des
attaquants distants de provoquer un déni de service et, éventuellement,
d’exécuter du code arbitraire à l'aide d'une longue commande SMTP sans caractère
espace, faisant qu’un tableau soit référencé avec un indice négatif.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2005-1515">CVE-2005-1515</a>

<p>Une erreur d’entiers signés dans les fonctions qmail_put et substdio_put dans
qmail, lors d’une exécution sur une plateforme 64 bits avec une grande
utilisation de mémoire virtuelle, permet à des attaquants distants de provoquer
un déni de service et, éventuellement, d’exécuter du code arbitraire à l'aide
d'un grand nombre de commandes SMTP RCPT TO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3811">CVE-2020-3811</a>

<p>La fonction qmail-verify comme utilisée dans netqmail 1.06 est prédisposée à
une vulnérabilité de contournement de vérification d’adresse de courriel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3812">CVE-2020-3812</a>

<p>La fonction qmail-verify comme utilisée dans netqmail 1.06 est prédisposée à
une vulnérabilité de divulgation d'informations. Un attaquant local peut
tester l’existence des fichiers et répertoires dans tout le système de fichiers
car qmail-verify est exécutée en tant qu’administrateur et teste l’existence de
fichiers dans le répertoire utilisateur « home » de l’attaquant, sans diminuer d’abord ses
privilèges.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.06-6.2~deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netqmail.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2234.data"
# $Id: $
