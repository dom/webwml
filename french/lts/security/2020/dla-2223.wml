#use wml::debian::translation-check translation="f2f16e9113c1120e6e08eb7fa1f2a7993e5ccf8b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le paquet salt, un logiciel
de gestion automatique de configuration et d’infrastructure.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11651">CVE-2020-11651</a>

<p>La classe ClearFuncs du processus salt-master ne validait pas correctement
les appels de méthode. Cela permettait à un utilisateur distant d’accéder à
quelques méthodes sans authentification. Ces méthodes pouvaient être utilisées
pour récupérer des jetons du salt-master ou exécuter des commandes arbitraires
sur des salt-minions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11652">CVE-2020-11652</a>

<p>La classe ClearFuncs du processus salt-master permettait d’accéder à quelques
méthodes qui ne nettoyaient pas correctement les chemins. Ces méthodes
permettaient un accès à des répertoires arbitraires aux utilisateurs
authentifiés.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2014.1.13+ds-3+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets salt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2223.data"
# $Id: $
