#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4473">CVE-2016-4473</a>
<p>Une libération de mémoire non valable peut survenir sous certaines
conditions lors du traitement d'archives compatibles avec phar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4538">CVE-2016-4538</a>
<p>La fonction bcpowmod dans ext/bcmath/bcmath.c dans PHP avant 5.5.35,
5.6.x avant 5.6.21 et 7.x avant 7.0.6 accepte un entier négatif pour
l'argument d'échelle. Cela permet à des attaquants distants de provoquer
un déni de service ou éventuellement d'avoir un autre impact non précisé à
l'aide d'un appel contrefait (déjà corrigé par le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4537">CVE-2016-4537</a>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5114">CVE-2016-5114</a>
<p>sapi/fpm/fpm/fpm_log.c dans PHP avant 5.5.31, 5.6.x avant 5.6.17 et 7.x
avant 7.0.2 interprète mal la sémantique de la valeur de retour de
snprintf. Cela permet à des attaquants d'obtenir des informations sensibles
à partir de la mémoire du processus ou de provoquer un déni de service
(lecture hors limites et dépassement de tampon) à l'aide d'une longue
chaîne, comme démontré par une longue URI dans une configuration avec une
journalisation personnalisée de REQUEST_URI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5399">CVE-2016-5399</a>
<p>Traitement d'erreur inadéquat dans bzread()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5768">CVE-2016-5768</a>
<p>Une vulnérabilité de double libération de mémoire dans la fonction
_php_mb_regex_ereg_replace_exec dans php_mbregex.c dans l'extension
mbstring dans PHP avant 5.5.37, 5.6.x avant 5.6.23 et 7.x avant 7.0.8
permet à des attaquants distants d'exécuter du code arbitraire ou de
provoquer un déni de service (plantage de l'application) en exploitant une
exception de rappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5769">CVE-2016-5769</a>
<p>Plusieurs dépassements d'entiers dans mcrypt.c dans l'extension mcrypt
dans PHP avant 5.5.37, 5.6.x avant 5.6.23 et 7.x avant 7.0.8 permettent à
des attaquants distants de provoquer un déni de service (dépassement de tas
et plantage de l'application) ou éventuellement d'avoir un autre impact non
précisé à l'aide d'une valeur de longueur contrefaite. Cela concerne les
fonctions (1) mcrypt_generic et (2) mdecrypt_generic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5770">CVE-2016-5770</a>
<p>Un dépassement d'entier dans la fonction SplFileObject::fread dans
spl_directory.c dans l'extension SPL dans PHP avant 5.5.37 et 5.6.x
avant 5.6.23 permet à des attaquants distants de provoquer un déni de
service ou éventuellement d'avoir un autre impact non précisé à l'aide d'un
long argument d'entier, un problème lié au
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5096">CVE-2016-5096</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5771">CVE-2016-5771</a>
<p>spl_array.c dans l'extension SPL dans PHP avant 5.5.37 et 5.6.x
avant 5.6.23 interagit incorrectement avec l'implémentation d'unserialize
et le ramasse-miettes. Cela permet à des attaquants distants d'exécuter du
code arbitraire ou de provoquer un déni de service (utilisation de mémoire
après libération et plantage de l'application) à l'aide de données
sérialisées contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5772">CVE-2016-5772</a>
<p>Une vulnérabilité de double libération dans la fonction
php_wddx_process_data dans wddx.c dans l'extension WDDX dans PHP
avant 5.5.37, 5.6.x avant 5.6.23 et 7.x avant 7.0.8 permet à des
attaquants distants de provoquer un déni de service (plantage de
l'application) ou éventuellement d'exécuter du code arbitraire à l'aide de
données XML contrefaites qui sont mal gérées dans un appel
wddx_deserialize.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5773">CVE-2016-5773</a>
<p>php_zip.c dans l'extension zip dans PHP avant 5.5.37, 5.6.x
avant 5.6.23 et 7.x avant 7.0.8 interagit incorrectement avec
l'implémentation d'unserialize et le ramasse-miettes. Cela permet à des
attaquants distants d'exécuter du code arbitraire ou de provoquer un déni
de service (utilisation de mémoire après libération et plantage de
l'application) grâce à des données sérialisées contrefaites contenant un
objet ZipArchive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6289">CVE-2016-6289</a>
<p>Un dépassement d'entier dans la fonction virtual_file_ex dans
TSRM/tsrm_virtual_cwd.c dans PHP avant 5.5.38, 5.6.x avant 5.6.24 et 7.x
avant 7.0.9 permet à des attaquants distants de provoquer un déni de
service (dépassement de pile) ou éventuellement d'avoir un autre impact non
précisé à l'aide d'une opération d'extraction contrefaite sur une archive
ZIP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6290">CVE-2016-6290</a>
<p>ext/session/session.c dans PHP avant 5.5.38, 5.6.x avant 5.6.24 et 7.x
avant 7.0.9 ne préserve pas correctement une certaine structure de données
de hachage. Cela permet à des attaquants distants de provoquer un déni de
service (utilisation de mémoire après libération) ou éventuellement d'avoir
un autre impact non précisé à l'aide de vecteurs liés à la
désérialisation de session.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6291">CVE-2016-6291</a>
<p>La fonction exif_process_IFD_in_MAKERNOTE dans ext/exif/exif.c dans PHP
avant 5.5.38, 5.6.x avant 5.6.24 et 7.x avant 7.0.9 permet à des
attaquants distants de provoquer un déni de service (accès hors limites à
un tableau et corruption de mémoire), d'obtenir des informations sensibles
de la mémoire d'un processus, ou éventuellement d'avoir un autre impact non
précisé à l'aide d'une image JPEG contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6292">CVE-2016-6292</a>
<p>La fonction exif_process_user_comment dans ext/exif/exif.c dans PHP
avant 5.5.38, 5.6.x avant 5.6.24 et 7.x avant 7.0.9 permet à des
attaquants distants de provoquer un déni de service (déréférencement de
pointeur NULL et plantage de l'application) à l'aide d'une image JPEG
contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6294">CVE-2016-6294</a>
<p>La fonction locale_accept_from_http dans
ext/intl/locale/locale_methods.c dans PHP avant 5.5.38, 5.6.x avant 5.6.24
et 7.x avant 7.0.9 ne restreint pas correctement les appels à la fonction
uloc_acceptLanguageFromHTTP d'ICU. Cela permet à des attaquants distants de
provoquer un déni de service (lecture hors limites) ou éventuellement
d'avoir un autre impact non précisé à l'aide d'un appel avec un argument
long.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6295">CVE-2016-6295</a>
<p>ext/snmp/snmp.c dans PHP avant 5.5.38, 5.6.x avant 5.6.24 et 7.x
avant 7.0.9 interagit incorrectement avec l'implémentation d'unserialize et
le ramasse-miettes. Cela permet à des attaquants distants de provoquer un
déni de service (utilisation de mémoire après libération et plantage de
l'application) ou éventuellement d'avoir un autre impact non précisé à
l'aide de données sérialisées contrefaites, un problème lié au
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5773">CVE-2016-5773</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6296">CVE-2016-6296</a>
<p>Une erreur de signe d'entier dans la fonction simplestring_addn dans
simplestring.c dans xmlrpc-epi jusqu'à la version 0.54.2, telle
qu'utilisée dans php avant 5.5.38, 5.6.x avant 5.6.24 et 7.x avant 7.0.9,
permet à des attaquants distants de provoquer un déni de service
(dépassement de pile) ou éventuellement d'avoir un autre impact non précisé
à l'aide d'un premier argument long pour la fonction xmlrpc_encode_request
de PHP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6297">CVE-2016-6297</a>
<p>Un dépassement d'entier dans la fonction php_stream_zip_opener dans
ext/zip/zip_stream.c dans PHP avant 5.5.38, 5.6.x avant 5.6.24 et 7.x
avant 7.0.9 permet à des attaquants distants de provoquer un déni de
service (dépassement de pile) ou éventuellement d'avoir un autre impact non
précisé à l'aide d'une adresse zip:// URL contrefaite.</p></li>

<li>Bogue – 70436
<p>Vulnérabilité d'utilisation de mémoire après libération dans
unserialize().</p></li>

<li>Bogue – 72681
<p>Une vulnérabilité d'injection de données de session PHP, consomme des
données même si elles ne sont pas stockées.</p></li>

</ul>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 5.4.45-0+deb7u5 de php5.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-628.data"
# $Id: $
