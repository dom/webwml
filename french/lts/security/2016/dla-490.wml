#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de sécurité ont été découvertes dans bozohttpd, un
petit serveur HTTP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5015">CVE-2014-5015</a>

<p>Le serveur HTTP Bozotic (ou bozohttpd), versions antérieures à 201407081,
tronque les chemins lors de la vérification des restrictions .htpasswd.
Cela permet à des attaquants distants de contourner le schéma
d'authentification HTTP et d'accéder aux restrictions à l'aide d'un long
chemin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8212">CVE-2015-8212</a>

<p>Un défaut dans la prise en charge de la gestion de suffixe de CGI a été
découvert, si l'option -C a été utilisée pour installer un gestionnaire CGI,
qui pourrait avoir pour conséquence l'exécution de code distant.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 20111118-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bozohttpd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-490.data"
# $Id: $
