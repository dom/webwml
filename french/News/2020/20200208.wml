#use wml::debian::translation-check translation="96c05360e385187167f1ccbce37d38ce2e5e6920" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.3</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la troisième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis à
niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction alot "Suppression des délais d'expiration des clés de l'ensemble de tests, corrigeant un échec de construction">
<correction atril "Correction d'erreur de segmentation quand aucun document n'est chargé ; correction de lecture de mémoire non initialisée [CVE-2019-11459]">
<correction base-files "Mise à jour pour cette version">
<correction beagle "Fourniture d'un script d'enveloppe plutôt que des liens symboliques vers les JAR, les rendant à nouveau opérationnels">
<correction bgpdump "Correction d'erreur de segmentation">
<correction boost1.67 "Correction d'un comportement indéfini menant au plantage de libboost-numpy">
<correction brightd "Comparaison réelle de la valeur lue à partir de /sys/class/power_supply/AC/online avec <q>0</q>">
<correction casacore-data-jplde "Inclusion des tables jusqu'à 2040">
<correction clamav "Nouvelle version amont ; correction d'un problème de déni de service [CVE-2019-15961] ; retrait de l'option ScanOnAccess, remplacée par clamonacc">
<correction compactheader "Nouvelle version amont compatible avec Thunderbird 68">
<correction console-common "Correction d'une régression qui menait à ce que des fichiers ne soient pas inclus">
<correction csh "Correction d'erreur de segmentation sur la commande eval">
<correction cups "Correction de fuite de mémoire dans ppdOpen ; correction de la validation de la langue par défaut dans ippSetValuetag [CVE-2019-2228]">
<correction cyrus-imapd "Ajout du type BACKUP à cyrus-upgrade-db, corrigeant des problèmes de mise à niveau">
<correction debian-edu-config "Réglages du mandataire conservés sur le client si WPAD est inaccessible">
<correction debian-installer "Reconstruction avec proposed-updates ; adaptation de la création de mini.iso sur arm de telle sorte que l'amorçage réseau EFI fonctionne ; mise à jour de unstable à Buster de USE_UDEBS_FROM par défaut pour aider les utilisateurs à réaliser des constructions locales">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-security-support "Mise à jour de l’état de la prise en charge du suivi de sécurité de divers paquets">
<correction debos "Reconstruction avec golang-github-go-debos-fakemachine mis à jour">
<correction dispmua "Nouvelle version amont compatible avec Thunderbird 68">
<correction dkimpy "Nouvelle version amont stable">
<correction dkimpy-milter "Correction de la gestion des privilèges au démarrage de telle sorte que les sockets Unix fonctionnent">
<correction dpdk "Nouvelle version amont stable">
<correction e2fsprogs "Correction d'un potentiel dépassement de pile par le bas dans e2fsck [CVE-2019-5188] ; correction d'utilisation de mémoire après libération dans e2fsck">
<correction fig2dev "Chaînes de texte Fig v2 se terminant par de multiples ^A permises [CVE-2019-19555] ; rejet des grands types de flèches provoquant un dépassement d'entier [CVE-2019-19746] ; correction de divers plantages [CVE-2019-19797]">
<correction freerdp2 "Correction de la gestion de valeur de retour de realloc [CVE-2019-17177]">
<correction freetds "tds : assurance qu'UDT a varint réglé à 8 [CVE-2019-13508]">
<correction git-lfs "Correction de problèmes de construction avec les dernières versions de Go">
<correction gnubg "Augmentation de la taille des tampons statiques utilisés pour les messages de construction lors du démarrage du programme de sorte que la traduction en espagnol ne provoque pas de dépassement de tampon">
<correction gnutls28 "Correction de problèmes d'interopérabilité avec gnutls 2.x ; correction d'analyse de certificats utilisant RegisteredID">
<correction gtk2-engines-murrine "Correction de co-installabilité avec d'autres thèmes">
<correction guile-2.2 "Correction d'échec de construction">
<correction libburn "Correction de <q>la gravure multipiste de cdrskin était lente et bloquait après la piste 1</q>">
<correction libcgns "Correction d'échec de construction sur ppc64el">
<correction libimobiledevice "Gestion correcte des écritures SSL partielles">
<correction libmatroska "Dépendance de la bibliothèque partagée haussée à 1.4.7 parce que cette version introduit de nouveaux symboles">
<correction libmysofa "Corrections de sécurité [CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095]">
<correction libole-storage-lite-perl "Correction de l'interprétation des années à partir de 2020">
<correction libparse-win32registry-perl "Correction de l'interprétation des années à partir de 2020">
<correction libperl4-corelibs-perl "Correction de l'interprétation des années à partir de 2020">
<correction libsolv "Correction de dépassement de tas [CVE-2019-20387]">
<correction libspreadsheet-wright-perl "Correction des feuilles de calcul OpenDocument précédemment inutilisables et du passage d'options de formatage de JSON">
<correction libtimedate-perl "Correction de l'interprétation des années à partir de 2020">
<correction libvirt "Apparmor : exécution de pygrub autorisée ; pas de rendu de osxsave et ospke dans la ligne de commande de QEMU ; cela aide les versions récentes de QEMU avec certaines configurations générées par virt-install">
<correction libvncserver "RFBserver : pas de fuite de mémoire de pile vers un attaquant distant [CVE-2019-15681] ; gel durant la clôture de connexion et erreur de segmentation résolus dans les serveurs VNC multifils ; correction d'un problème de connexion aux serveurs VMWare ; correction de plantage de x11vnc lors de la connexion de vncviewer">
<correction limnoria "Correction de divulgation d'informations et d'exécution potentielle de code à distance dans le greffon Math [CVE-2019-19010]">
<correction linux "Nouvelle version amont stable">
<correction linux-latest "Mise à jour de l'ABI du noyau Linux 4.19.0-8">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction mariadb-10.3 "Nouvelle version amont stable [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa "Appel shmget() avec les permissions 0600 à la place de 0777 [CVE-2019-5068]">
<correction mnemosyne "Ajout de dépendance manquante à PIL">
<correction modsecurity "Correction d'un bogue d'analyse d'en-tête de cookie [CVE-2019-19886]">
<correction node-handlebars "Appel direct de <q>helperMissing</q> et <q>blockHelperMissing</q> interdit [CVE-2019-19919]">
<correction node-kind-of "Correction d'une vulnérabilité de vérification de type dans ctorName() [CVE-2019-20149]">
<correction ntpsec "Correction de la lenteur des nouveaux essais DNS ; correction de ntpdate -s (syslog) pour corriger l'accroche (hook) if-up ; corrections de la documentation">
<correction numix-gtk-theme "Correction de co-installabilité avec d'autres thèmes">
<correction nvidia-graphics-drivers-legacy-340xx "Nouvelle version amont stable">
<correction nyancat "Reconstruction dans un environnement propre pour ajouter un fichier unit de systemd pour nyancat-server">
<correction openjpeg2 "Correction de dépassement de tas [CVE-2018-21010] et de dépassement d'entier [CVE-2018-20847]">
<correction opensmtpd "Avertissement des utilisateurs de la modification de la syntaxe de smtpd.conf (dans les versions antérieures) ; installation de smtpctl setgid opensmtpq ; gestion du code de retour non nul à partir du nom d'hôte durant la phase de configuration">
<correction openssh "Déni d'IPC (non fatal) dans le bac à sable seccomp, correction d'échecs avec OpenSSL 1.1.1d et Linux &lt;3.19 sur certaines architectures">
<correction php-horde "Correction d'un problème de script intersite stocké dans Cloud Block de Horde [CVE-2019-12095]">
<correction php-horde-text-filter "Correction d'expressions rationnelles non valables">
<correction postfix "Nouvelle version amont stable">
<correction postgresql-11 "Nouvelle version amont stable">
<correction print-manager "Correction de plantage si CUPS renvoie le même identifiant pour plusieurs tâches d'impression">
<correction proftpd-dfsg "Correction de problèmes de CRL [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke "Correction du chemin vers les fontes">
<correction python-evtx "Correction d'importation de <q>hexdump</q>">
<correction python-internetarchive "Fermeture du fichier après obtention du hachage évitant l'épuisement du descripteur de fichier">
<correction python3.7 "Corrections de sécurité [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src "Ajout de la prise en charge des imprimantes non PPD et repli silencieux évité pour une imprimante prenant en charge PPD ; correction de plantage lors de l'utilisation de QLabels avec le format « Rich Text » ; correction des événements de survol (hover) des tablettes graphiques">
<correction qtwebengine-opensource-src "Correction d'analyse de PDF ; désactivation de la pile exécutable">
<correction quassel "Correction des dénis d'AppArmor de quasselcore lors de la sauvegarde de la configuration ; correction du canal par défaut pour Debian ; retrait d'un fichier NEWS inutile">
<correction qwinff "Correction d'un plantage dû à une détection incorrecte de fichier">
<correction raspi3-firmware "Correction de la détection de la console série avec les noyaux 5.x">
<correction ros-ros-comm "Correction de problèmes de sécurité [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube "Nouvelle version amont stable ; correction de permissions non sûres dans le greffon enigma [CVE-2018-1000071]">
<correction schleuder "Correction de la reconnaissance des mots clés dans les courriels avec <q>protected headers</q> et un sujet vide ; retrait des signatures qui ne sont pas des auto-signatures lors du rafraîchissement ou de la récupération de clés ; erreur si l'argument fourni à « refresh_keys » n'est pas une liste existante ; ajout de l'en-tête List-Id absent pour les messages de notification envoyés aux administrateurs ; gestion élégante des problèmes de déchiffrement ; passage à l'encodage ASCII-8BIT par défaut">
<correction simplesamlphp "Correction d'incompatibilité avec PHP 7.3">
<correction sogo-connector "Nouvelle version amont compatible avec Thunderbird 68">
<correction spf-engine "Correction de la gestion des privilèges au démarrage de telle sorte que les sockets Unix fonctionnent ; mise à jour le la documentation pour TestOnly">
<correction sudo "Correction d'un dépassement de tampon (non exploitable dans Buster) quand pwfeedback est activé et que l'entrée n'est pas un périphérique tty [CVE-2019-18634]">
<correction systemd "Réglage de fs.file-max sysctl à LONG_MAX plutôt qu'à ULONG_MAX ; modification de l’appartenance et du mode des répertoires d'exécution également pour les utilisateurs statiques, assurant que les répertoires d'exécution comme CacheDirectory et StateDirectory ont été correctement affectés à l'utilisateur spécifié dans User= avant de charger le service">
<correction tifffile "Correction de script d'enveloppe">
<correction tigervnc "Corrections de sécurité [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Corrections de sécurité [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif "Correction des chemins vers ip(6)tables-restore au vu de la migration vers nftables">
<correction unhide "Correction d'épuisement de pile">
<correction x2goclient "Retrait de ~/, ~user{,/}, ${HOME}{,/} et $HOME{,/} des chemins de destination en mode SCP ; corrections de régression avec les dernières versions de libssh avec application des correctifs pour le CVE-2019-14889">
<correction xmltooling "Correction d'une situation de compétition qui pourrait mener à un plantage en charge">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction caml-crush "[armel] non constructible du fait de l'absence d'ocaml-native-compilers">
<correction firetray "Incompatible avec les versions actuelles de Thunderbird">
<correction koji "Problèmes de sécurité">
<correction python-lamson "Cassé par des modifications dans python-daemon">
<correction radare2 "Problèmes de sécurité ; l'amont n'offre pas de prise en charge stable">
<correction radare2-cutter "Dépend de radare2 qui doit être retiré">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>


<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>



