#use wml::debian::translation-check translation="a6e124e3bdf8bba46778a528df9a1e871c2f4b94"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.2</define-tag>
<define-tag release_date>2019-11-16</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la segunda actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad,
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction aegisub "Corrige caída al seleccionar un idioma de la parte inferior de la lista <q>Spell checker language</q> («Idioma del corrector ortográfico»); corrige caída al clicar con el botón derecho en la caja de texto de subtítulos">
<correction akonadi "Corrige varias caídas y problemas de abrazos mortales">
<correction base-files "Actualiza /etc/debian_version para esta versión">
<correction capistrano "Corrige fallo de borrado de versiones antiguas cuando hay demasiadas">
<correction cron "Deja de usar API obsoleta de SELinux">
<correction cyrus-imapd "Corrige pérdida de datos al actualizar desde la versión 3.0.0 o anterior">
<correction debian-edu-config "Gestiona ficheros de configuración de Firefox ESR más recientes; añade condicionalmente sentencia post-up a la entrada eth0 en /etc/network/interfaces">
<correction debian-installer "Corrige tipos de letra ilegibles en pantallas hidpi en imágenes para arranque en red («netboot») arrancadas con EFI">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction distro-info-data "Añade Ubuntu 20.04 LTS, Focal Fossa">
<correction dkimpy-milter "Nueva versión «estable» del proyecto original; corrige soporte de sysvinit; captura más errores de codificación ASCII para aumentar la resiliencia frente a datos erróneos; corrige la extracción de mensajes de forma que funcione correctamente la firma y la verificación en la misma pasada por el milter">
<correction emacs "Actualiza la clave de empaquetado del EPLA">
<correction fence-agents "Corrige eliminación incompleta de fence_amt_ws">
<correction flatpak "Nueva versión «estable» del proyecto original">
<correction flightcrew "Correcciones de seguridad [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk "Corrige selección demasiado agresiva de los tipos de letra Noto CJK en navegadores web modernos con la configuración regional china">
<correction freetype "Gestiona correctamente los «phantom points» en tipos de letra con «hints» variables">
<correction gdb "Recompilado con la libbabeltrace nueva, con número de versión mayor para evitar entrar en conflicto con la versión subida anteriormente">
<correction glib2.0 "Se asegura de que los clientes de libdbus pueden autenticarse con un GDBusServer como el de ibus">
<correction gnome-shell "Nueva versión «estable» del proyecto original; corrige truncamiento de mensajes largos en diálogos de Shell-modal; evita caída al reasignar actores muertos">
<correction gnome-sound-recorder "Corrige caída al seleccionar una grabación">
<correction gnustep-base "Inhabilita el daemon gdomap que se habilitó accidentalmente en actualizaciones desde stretch">
<correction graphite-web "Elimina función no utilizada <q>send_email</q> [CVE-2017-18638]; evita errores cada hora en cron cuando no hay base de datos whisper">
<correction inn2 "Corrige la negociación de conjuntos de algoritmos de cifrado de DHE">
<correction libapache-mod-auth-kerb "Corrige fallo de «uso tras liberar» que provoca caída">
<correction libdate-holidays-de-perl "Marca el Día internacional del niño (20 de sep) como festivo en Turingia de 2019 en adelante">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libofx "Corrige problema de desreferencia de puntero nulo [CVE-2019-9656]">
<correction libreoffice "Corrige el controlador de postgresql con PostgreSQL 12">
<correction libsixel "Corrige varios problemas de seguridad [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt "Corrige puntero colgante en xsltCopyText [CVE-2019-18197]">
<correction lucene-solr "Inhabilita llamada obsoleta a ContextHandler en solr-jetty9.xml; corrige permisos de Jetty en índice de SOLR">
<correction mariadb-10.3 "Nueva versión «estable» del proyecto original">
<correction modsecurity-crs "Corrige reglas de subida («upload») de script PHP [CVE-2019-13464]">
<correction mutter "Nueva versión «estable» del proyecto original">
<correction ncurses "Corrige varios problemas de seguridad [CVE-2019-17594 CVE-2019-17595] y otros problemas en tic">
<correction ndppd "Evita fichero de PID con permisos universales de escritura que rompía scripts de inicio de daemons">
<correction network-manager "Corrige permisos de los ficheros <q>/var/lib/NetworkManager/secret_key</q> y /var/lib/NetworkManager">
<correction node-fstream "Corrige problema de sobrescritura de ficheros arbitrarios [CVE-2019-13173]">
<correction node-set-value "Corrige contaminación de prototipo [CVE-2019-10747]">
<correction node-yarnpkg "Fuerza el uso de HTTPS para registros normales">
<correction nx-libs "Corrige regresiones introducidas en el envío («upload») anterior, que afectan a x2go">
<correction open-vm-tools "Corrige fugas del contenido de la memoria y la gestión de errores">
<correction openvswitch "Actualiza debian/ifupdown.sh para permitir la modificación de MTU; corrige las dependencias con Python para usar Python 3">
<correction picard "Actualiza traducciones para corregir caída con la configuración regional española">
<correction plasma-applet-redshift-control "Corrige el modo manual al usarlo con versiones de redshift superiores a la 1.12">
<correction postfix "Nueva versión «estable» del proyecto original; soluciona provisionalmente el mal rendimiento del TCP loopback">
<correction python-cryptography "Corrige errores del conjunto de pruebas cuando se compila con versiones de OpenSSL más recientes; corrige una fuga de contenido de la memoria que se puede desencadenar al analizar sintácticamente extensiones de certificados x509, como AIA">
<correction python-flask-rdf "Añade Depende de python{3,}-rdflib">
<correction python-oslo.messaging "Nueva versión «estable» del proyecto original; corrige conmutación del destino de la conexión cuando desaparece un nodo de un cluster rabbitmq">
<correction python-werkzeug "Asegura que los contenedores Docker tienen PIN de depurador únicos [CVE-2019-14806]">
<correction python2.7 "Corrige varios problemas de seguridad [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota "Corrige rotación de rpc.rquotad con la CPU al 100%">
<correction rpcbind "Permite que se habiliten en tiempo de ejecución las llamadas remotas">
<correction shelldap "Repara las autenticaciones SASL, añade una opción «sasluser»">
<correction sogo "Corrige la visualización de correos electrónicos firmados con PGP">
<correction spf-engine "Nueva versión «estable» del proyecto original; corrige soporte de sysvinit">
<correction standardskriver "Corrige aviso de depreciación desde config.RawConfigParser; usa la orden externa <q>ip</q> en lugar de la depreciada <q>ifconfig</q>">
<correction swi-prolog "Usa HTTPS al contactar con servidores de paquetes del proyecto original">
<correction systemd "core: no propaga nunca errores al recargar («reload») al resultado del servicio; corrige error de sync_file_range en contenedores nspawn en arm, ppc; corrige que RootDirectory no funcione cuando se usa en combinación con User; asegura que los controles de acceso sobre la interfaz D-Bus de systemd-resolved se hacen cumplir correctamente [CVE-2019-15718]; corrige StopWhenUnneeded=true para unidades mount; hace que MountFlags=shared funcione de nuevo">
<correction tmpreaper "Evita la rotura de servicios systemd que usan PrivateTmp=true">
<correction trapperkeeper-webserver-jetty9-clojure "Restaura la compatibilidad SSL con versiones de Jetty más recientes">
<correction tzdata "Nueva versión del proyecto original">
<correction ublock-origin "Nueva versión del proyecto original, compatible con Firefox ESR68">
<correction uim "Restablece libuim-data como paquete de transición, corrigiendo algunos problemas tras la actualización a buster">
<correction vanguards "Nueva versión «estable» del proyecto original; evita una recarga de la configuración de tor mediante SIGHUP que provoca denegación de servicio de protecciones de vanguards">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction firefox-esr "[armel] No se puede seguir soportando debido a dependencias de compilación con nodejs">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


