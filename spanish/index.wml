#use wml::debian::mainpage title="El sistema operativo universal" GEN_TIME="yes"
#use wml::debian::translation-check translation="db28b03ffeea30dd379cb4120e6f83d0e85722d8" maintainer="Javier Fernandez-Sanguino <jfs@debian.org>"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<span class="download"><a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">Descargar Debian <current_release_short><em>(instalador por red para PC de 64 bits)</em></a> </span>


<div id="splash">
   <h1>Debian</h1>
</div>

<div id="intro">
<p>Debian es un sistema operativo (S.O.) <a
href="intro/free">libre</a>, para su computadora.  El sistema operativo es el
conjunto de programas básicos y utilidades que hacen que funcione su
computadora.
</p>

<p>Debian ofrece más que un S.O. puro; viene con <packages_in_stable>
<a href="distrib/packages">paquetes</a>, programas precompilados distribuidos
en un formato que hace más fácil la instalación en su computadora.
<a href="intro/about">Lea más...</a></p>
</div>

<hometoc/>

<p class="infobar">La <a href="releases/stable/">última versión estable de Debian</a> es
la <current_release_short>. La última actualización de esta versión se publicó el
<current_release_date>. Puede leer más sobre
<a href="releases/">las versiones disponibles de Debian</a>.</p>

<h2>Primeros pasos</h2>

<p>Use la barra de navegación de la parte superior de esta página para acceder a más contenido.</p>
<p>Además, los usuarios que hablan idiomas distintos del inglés pueden revisar la sección
<a href="international/">internacional</a>, y las personas que usan sistemas distintos
de Intel x86 deberían revisar la sección de <a href="ports/">adaptaciones</a>.</p>

<hr />
<a class="rss_logo" href="News/news">RSS</a>
<h2>Últimas noticias</h2>
<p><:= get_recent_list ('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)','','\d+\w*') :>

<p>Si busca noticias más antiguas, consulte la <a href="$(HOME)/News/">página
de noticias</a>. Si desea recibir correo siempre que surja una nueva noticia,
suscríbase a la <a href="MailingLists/debian-announce">lista de correo debian-announce</a>.</p>

<hr />
<a class="rss_logo" href="security/dsa">RSS</a>
<h2>Últimos avisos de seguridad</h2>

<p><:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :></p>

<p>Si busca avisos más antiguos, consulte la <a href="$(HOME)/security/">página
de seguridad</a>. Si desea recibir los avisos de seguridad en el
momento de su publicación, suscríbase a la <a
href="https://lists.debian.org/debian-security-announce/">lista de correo
debian-security-announce</a>.</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Noticias de Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Noticias del Proyecto Debian" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (solo títulos)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (resúmenes)" href="security/dsa-long">
:#rss#}
