#use wml::debian::template title="Napište nám" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="e8db1e3feeb43ec6a36cd34b0b7a301157ee5e02" maintainer="Michal Simunek"

# Korektura:   Martin Vlk  verze 1.40
# Korektura:    Branislav Makúch 07/2020

<p>Debian je rozsáhlou organizací a&nbsp;nabízí se mnoho cest
jak nás kontaktovat. Tato stránka shrnuje pouze nejžádanější kontakty; není v žádném případě vyčerpávající.
Další najdete na jednotlivých webových stránkách Debianu.</p>

<p>
Vezměte na vědomí, že většina uvedených e-mailových adres
jsou otevřené poštovní konference s veřejnými archivy.
Přečtěte si, prosím, <a href="$(HOME)/MailingLists/disclaimer">\
závazné prohlášení</a> než pošlete jakékoliv zprávy.
</p>

<p>Společným jazykem pro komunikaci s&nbsp;vývojáři Debianu je angličtina.
Žádáme proto, aby alespoň úvodní dotazy vývojářům byly pokládány
v&nbsp;<strong>angličtině</strong>. Pokud to není možné, projděte si
<a href="https://lists.debian.org/users.html#debian-user">uživatelskou
e-mailovou konferenci</a> ve vašem jazyce.</p>



<ul class="toc">
  <li><a href="#generalinfo">Všeobecné informace</a>
  <li><a href="#installuse">Instalace a&nbsp;používání Debianu</a>
  <li><a href="#press">Propagace / Tisk</a>
  <li><a href="#events">Události / Konference</a>
  <li><a href="#helping">Pomoc Debianu</a>
  <li><a href="#packageproblems">Hlášení chyb v&nbsp;balíčcích Debianu</a>
  <li><a href="#development">Vývoj Debianu</a>
  <li><a href="#infrastructure">Problémy s&nbsp;infrastrukturou Debianu</a>
  <li><a href="#harassment">Problémy s obtěžováním</a>
</ul>

<h2 id="generalinfo">Všeobecné informace</h2>

<p>Většina informací o&nbsp;Debianu je soustředěna na webových stránkách
<a href="$(HOME)">https://www.debian.org/</a>. Proto si je, před tím než
nás budete kontaktovat, projděte
a&nbsp;<a href="$(SEARCH)">prohledejte</a>.

<p>Stránky s <a href="doc/user-manuals#faq/">často kladenými dotazy</a> přímo zodpovídají mnoho otázek uživatelů.

<p>Všeobecné otázky ohledně projektu Debian můžete posílat do e-mailové
konference <em>debian-project</em> na adrese
<email debian-project@lists.debian.org>. Neposílejte sem prosím otázky
o&nbsp;používání Linuxu - více k&nbsp;tomuto problému níže.


<h2 id="installuse">Instalace a&nbsp;používání Debianu</h2>

<p>Pokud víte jistě, že dokumentace na instalačním médiu ani webové stránky
neobsahují řešení vašeho problému, existuje velmi aktivní uživatelská
konference <em>debian-user</em>, kde mohou vaše dotazy zodpovědět uživatelé
a&nbsp;vývojáři Debianu. Do této konference posílejte všechny otázky
ohledně
<ul>
    <li>instalace
    <li>konfigurace
    <li>podporovaného hardwaru
    <li>administrace počítače
    <li>používání Debianu
</ul>
Po <a href="https://lists.debian.org/debian-user/">přihlášení</a>
do konference <em>debian-user</em> otázky posílejte na adresu
<email debian-user@lists.debian.org>.

<p>Navíc jsou zde uživatelské e-mailové konference v&nbsp;různých jazycích.
Viz informace o&nbsp;<a href="https://lists.debian.org/users.html#debian-user">přihlašování</a> do mezinárodních e-mailových konferencí.

<p>E-mailové konference si také můžete prohlížet formou diskuzních skupin
za použití webových rozhraní jako <a href="http://groups.google.com/">Google</a>.

<p>Pokud si myslíte, že jste našli chybu v&nbsp;instalačním systému, pošlete
o&nbsp;tom zprávu na adresu <email debian-boot@lists.debian.org>
nebo <a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">zašlete chybové hlášení</a> vůči pseudobalíčku
<a href="https://bugs.debian.org/debian-installer">debian-installer</a>.


<h2 id="press">Propagace / Tisk</h2>

<p>Pokud chcete získat informace pro své články nebo přispět zprávou na naše
zpravodajské stránky, kontaktujte
<a href="mailto:press@debian.org">tiskové oddělení</a> Debianu.


<h2 id="events">Události / Konference</h2>

<p>Pozvání na <a href="$(HOME)/events/">konference</a>, předváděcí akce
či události jiného druhu posílejte na
<a href="mailto:events@debian.org">oddělení událostí</a>. Žádosti
o&nbsp;letáky, plakáty a&nbsp;účast na akcích v&nbsp;Evropě posílejte
do evropské e-mailové
<a href="mailto:debian-events-eu@lists.debian.org">konference
o&nbsp;událostech</a>.


<h2 id="helping">Pomoc Debianu</h2>

<p>Pokud byste se chtěli podílet na Debianu, seznamte se prosím nejprve
s&nbsp;možnými <a href="devel/join/">způsoby pomoci</a>.

<p>Pokud chcete poskytovat zrcadlo Debianu, podívejte se na
<a href="mirror/">stránky o&nbsp;zrcadlení</a>. Nová zrcadla se oznamují
<a href="mirror/submit">webovým formulářem</a>. Problémy s&nbsp;existujícími
zrcadly lze hlásit na adrese <email mirrors@debian.org>.

<p>Zájemci o&nbsp;prodej CD Debianu najdou informace na <a
href="CD/vendors/info">stránkách pro prodejce CD</a>. Chcete-li být
na seznamu prodejců, prosím <a
href="CD/vendors/adding">vyplňte následující formulář</a>.


<h2 id="packageproblems">Hlášení chyb v&nbsp;balíčcích Debianu</h2>

<p>Za účelem hlášení chyb v&nbsp;balíčcích Debianu je k&nbsp;dispozici systém
pro sledování chyb, kde můžete daný problém snadno zaznamenat.
Přečtěte si prosím <a href="Bugs/Reporting">instrukce k&nbsp;zasílání
chybových zpráv</a>.

<p>Pokud chcete raději kontaktovat přímo správce balíčku, můžete použít
pro tento účel určenou e-mailovou adresu. E-maily zaslané na adresu
&lt;<var>jméno balíčku</var>&gt;@packages.debian.org budou přeposlány
správci zodpovědnému za daný balíček.

<p>Diskrétně uvědomit vývojáře o&nbsp;bezpečnostním problému v&nbsp;Debianu
můžete zasláním e-mailu na adresu <email security@debian.org>.


<h2 id="development">Vývoj Debianu</h2>

<p>Pokud máte otázky zaměřené spíše na vývoj, je zde několik <a
href="https://lists.debian.org/devel.html">e-mailových konferencí</a>
ve kterých lze kontaktovat vývojáře Debianu.

<p>Obecná e-mailová konference pro vývojáře je <em>debian-devel</em>.
Můžete se do ní <a href="https://lists.debian.org/debian-devel/">přihlásit</a>
a&nbsp;následně poslat e-mail na adresu
<email debian-devel@lists.debian.org>.


<h2 id="infrastructure">Problémy s&nbsp;infrastrukturou Debianu</h2>

<p>Problém se službou Debianu lze obvykle
<a href="Bugs/Reporting">ohlásit</a> jako chybu v&nbsp;odpovídajícím
<a href="Bugs/pseudo-packages">pseudobalíčku</a>.</p>

<p>Jinak nás můžete kontaktovat e-mailem.</p>

<define-tag btsurl>balíček: <a
href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Editoři webových stránek</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Překladatelé webových stránek</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Administrátoři systému emailových konferencí a správci archívů</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>Administrátoři systému pro hlášení chyb</dt>
  <dd><btsurl bugs.debian.org><br />
      <email owner@bugs.debian.org></dd>
</dl>

<h2 id="harassment">Problémy s obtěžováním</h2>

<p>Debian je komunita lidí, kteří si váží respektu a dialogu. Pokud jste obětí jakéhokoliv chování, které vám škodí, či pokud máte pocit, že jste byli obtěžování, ať už během konference, či sprintu organizovaného projektem, nebo v průběhu obecné spolupráce na projektu, prosím, kontaktujte Komunitní tým na: <email community@debian.org>.</p>

<p>Také máme <a href="intro/organization">úplný seznam oddělení</a>
spolu s&nbsp;e-mailovými adresami, na kterých lze kontaktovat
jejich členy.</p>
