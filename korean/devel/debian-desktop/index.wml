#use wml::debian::template title=" 데비안 데스크톱"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896" maintainer="Sebul"

<h2>데스크톱으로써 세계적 운영체제</h2>

<p>데비안 데스크톱 서브프로젝트는 집과 회사 워크스테이션 사용을 위한 가장 좋은 
운영체제를 만들고 싶어하는 자원봉사자 그룹입니다.
우리의 모토는 <q>Software that Just Works</q>.
간단히, 우리의 목표는 데비안, GNU, 그리고 리눅스를 주류 세계로 가져오는 겁니다.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop"/>
</p>
<h3>우리의 원칙</h3>
<ul>
  <li><a href="https://wiki.debian.org/DesktopEnvironment">데스크톱 환경</a>이 많음을 인식하고, 
  그걸 사용하는걸 지원하고, 데비안에서 잘 동작할 것을 확신합니다.
  </li>
  <li>우리는 오직 두 중요한 사용자 클래스만 있음을 압니다:
초보, 그리고 전문가.
우리는 초보자가 매우 쉽게 하도록 모든 것을 하며, 전문가가 사물을 조절할 수 있습니다.
  </li>
  <li>소프트웨어가 가장 일반적인 데스크톱 용으로 설정되도록 할 겁니다.
예를 들어, 설치동안 더해지는 일반적 사용자 계정은 sudo를 통해 오디오, 비디오를 실행시키고, 프린트, 
시스템 관리 권한이 있어야 합니다.
  </li>
  <li>
    <p>
우리는 최소한의 컴퓨터 지식으로도 사용자에게 묻는 질문 (최소한으로 유지되어야 함)이 이해되도록 노력할 것입니다.
오늘날 일부 데비안 패키지는 사용자에게 어려운 기술적 세부 사항을 제공합니다.
debian-installer가 사용자에게 제공하는 기술적 debconf 질문은 피해야합니다.
초보자에게는 종종 혼란스럽고 무섭습니다.
전문가에게는 귀찮고 불필요할 수 있습니다.
초보자는 이러한 질문에 대해 모를 수도 있습니다.
전문가는 데스크톱 환경을 구성할 수 있지만 설치 후 원하는 환경을 구성할 수 있습니다.
이러한 종류의 Debconf 질문의 우선 순위는 최소한 낮추어야 합니다.
    </p>
  <li>
  그리고 우리는 이 모든 걸 재미있게 할 겁니다 !
  </li>
</ul>
<h3>어떻게 도울까</h3>
<p>데비안 서브프로젝트의 가장 중요한 부분은 메일링 리스트, 웹 페이지, 또는 
패키지 아카이브 공간이 아닙니다.
가장 주요한 부분은 일을 일으키는 <em>동기부여된 사람들</em>입니다.
패키지와 패치를 만들기 위해서는 공식 개발자가 아니어도 됩니다.
데비안 데스크톱 핵심 팀이 여러분의 일이 통합되었는지 확인합니다.
그래서 여러분이 할 수 있는 몇 가지가 여기 있습니다:
</p>
<ul>
  <li>
  <q>데스크톱 기본환경</q>(또는 kde-desktop task)을 테스트하고, 
  <a href="$(DEVEL)/debian-installer/">다음 릴리스 테스트 이미지</a> 중 하나를 설치하고, 
  <a href="https://lists.debian.org/debian-desktop/">debian-desktop mailing list</a>로 피드백을 보내세요.
  </li>
  <li><a href="$(DEVEL)/debian-installer/">debian-installer</a>에서 작업하세요.  
GTK+ frontend는 여러분이 필요합니다.
  </li>
  <li>
  <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME 팀</a>,
<a href="https://qt-kde-team.pages.debian.net/">Debian Qt 및 KDE 팀</a> 또는
<a href="https://salsa.debian.org/xfce-team/">Debian Xfce Group</a>을 도와주세요.
여러분이 패키징, 버그 조사, 문서화, 테스트 그리고 더 많은 것을 도울 수 있습니다.
  </li>
  <li>사용자들에게 우리가 지금 갖고 있는 데스크톱(desktop, gnome-desktop 그리고 kde-desktop) 작업을 어떻게 설치하고 사용하는지 가르쳐 주세요.
  </li>
  <li>불필요한 <a href="https://packages.debian.org/debconf">debconf</a> 프롬프트의 
우선순위를 낮추거나 패키지에서 없애고, 필요한 것을 이해하기 쉽게 만드세요
  </li>
  <li><a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian  Desktop Artwork effort</a>를 도와 주세요.
  </li>
</ul>
<h3>위키</h3>
<p>우리 위키에 몇 개 글이 있으며, 우리의 시작점은: 
  <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>입니다.
어떤 Debian  Desktop 위키 글은 오래되었습니다.
</p>
<h3>메일링 리스트</h3>
<p>이 서브프로젝트는 메일링 리스트
<a href="https://lists.debian.org/debian-desktop/">debian-desktop</a> 에서 토론합니다.
</p>
<h3>IRC 채널</h3>
<p>
데비안 데스크톱에 관심 있는 사람(데비안 개발자 이건 아니건)은
OFTC IRC (irc.debian.org)의 # debian-desktop에 참여하세요.
</p>
<h3>누가 관련되었나요?</h3>
<p>누구나 환영합니다. 사실, pkg-gnome, pkg-kde 및 pkg-xfce 그룹의 모든 사람이 간접적으로
관련됩니다. debian-desktop 메일링 리스트 가입자는 활동적인 기여자입니다.
데비안 설치 프로그램과 tasksel 그룹은 우리의 목표로도 중요합니다.
</p>

<p>이 웹 페이지는 <a href="https://people.debian.org/~stratus/">Gustavo Franco</a>가 관리합니다.
이전 관리자는
  <a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a>와
  <a href="https://people.debian.org/~walters/">Colin Walters</a> 였습니다.
</p>
