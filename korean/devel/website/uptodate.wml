#use wml::debian::template title="웹 사이트 번역을 최신으로 유지하기"
#use wml::debian::translation-check translation="8f2dd37edbf6287df4029e3f234798efbcce2862" maintainer="Seunghun Han (kkamagui)"

<P>웹 페이지는 변하지 않는 정적인 상태가 아니므로, 특정 번역이 참조하는 원본을
추적하는 것과 마지막 번역 이후 어떤 페이지가 변경되었는지 검사하기 위해 이 정보를
사용하는 것은 좋은 생각입니다.
이 정보는 문서의 꼭대기(다른 "use" 헤더 아래)에 이런 형식으로 들어갑니다:

<pre>
\#use wml::debian::translation-check translation="git_commit_hash"
</pre>

<p>
여기서 <var>git_commit_hash</var>는 번역 파일이 번역했던 원본(영어) 파일의 Git
커밋 해시(Commit Hash)입니다.
여러분은 특정 커밋의 상세 정보를 <code>git show</code> 도구,
즉 <code>git show &lt;git_commit_hash&gt;</code>를 써서 얻을 수 있습니다.
여러분이 <kbd>copypage.pl</kbd> 스크립트를 webml 디렉터리에서 사용했다면, 이 줄은
여러분의 번역된 페이지의 초기 버전에 자동으로 더해집니다. 번역한 때의 원본 파일
버전을 가리키기 위해서 말이죠.
</p>

<p>원본 언어(영어) 파일이 변경되었음에도 불구하고, 몇몇 번역은 꽤 오랫동안
업데이트 되지 않을 수 있습니다.
내용 협상(Content Negotiation) 덕분에 번역된 페이지의 독자는 이를 모를 수 있고,
원문의 새 버전에 소개된 중요한 정보를 놓칠 수 있습니다.
<code>translation-check</code> 템플릿에는 여러분의 번역이 오래 되었는지 확인하고,
사용자에게 그것을 경고하는 적절한 메시지 출력 코드가 포함되어 있습니다.</p>

<P> <code>translation-check</code> 줄에 여러분이 사용할 수 있는 파라미터는 다음과
같습니다:

<dl>
 <dt><code>original="<var>language</var>"</code>
 <dd>여기서 <var>language</var>는 영어가 아니라면 여러분이 번역을 시작할
 언어명입니다.
 언어명은 VCS안의 최상위 서브디렉터리 이름과 반드시 같아야 하며,
 <code>languages.wml</code> 템플릿 안의 이름과도 반드시 같아야 합니다.

 <dt><code>mindelta="<var>number</var>"</code>
 <dd>mindeta는 번역이 <strong>오래 되었다고</strong> 간주하는 Git 변경(revision)의
 최대 차이값을 정의합니다.
 기본 값은 <var>1</var>입니다.
 덜 중요한 페이지를 위해서는 <var>2</var>로 설정하세요. 이는 번역이 오래되었다고
 간주하는데 두 개의 변경이 필요하다는 의미입니다.

 <dt><code>maxdelta="<var>number</var>"</code>
 <dd>maxdelta는 번역의 유효기간이 지났다고 간주하는 Git 변경(revision)의 최대
 차이값을 정의합니다.
 기본 값은 <var>5</var>입니다.
 가장 중요한 페이지를 위해서는 더 작게 설정합니다.
 <var>1</var>은 변경이 생길 때마다 번역의 유효기간이 지났다고 간주한다는 의미입니다.
</dl>

<p>번역이 오래된 것을 추적하는 일은 우리가 오래된 번역과 관련된 보고서인
<a href="stats/">번역 통계</a>를 만들 수 있게 합니다. 번역 통계에는 번역되지
않은 페이지의 리스트와 함께 파일 간의 차이와 관련된 유용한 링크가 있습니다.
이러한 정보는 번역자를 돕기 위한 것이며 새로운 사람들이 돕도록 유도하기
위함입니다. </p>

<p>
우리 사용자들에게 유효기간이 오래 지난 정보가 표시되는 것을 피하려고,
원본 파일이 변경된 후 6개월 이상 업데이트되지 않은 번역을 자동으로 제거합니다.
<a href="https://www.debian.org/devel/website/stats/"> 유효기간이 지난 번역 목록</a>에서
사라질 위기에 놓인 페이지를 살펴봐주세요.
</p>

<P>또한, webwml/ 디렉터리에 <kbd>check_trans.pl</kbd> 스크립트가 있는데, 여러분에게
업데이트가 필요한 페이지의 보고서를 보여줄겁니다.

<pre>
check_trans.pl <var>language</var>
</pre>

<P> 여기서 <var>language</var>는 여러분의 번역이 담긴 디렉터리 이름입니다.
예를 들어 "korean" 같은 것이죠.

<P> 번역이 안된 페이지는 "<code>Missing <var>filename</var></code>"처럼 표시되며,
원본을 따라가지 못한 페이지는
"<code>NeedToUpdate <var>filename</var> to version <var>x.y</var></code>"로
표시될 겁니다.

<P> 여러분이 정확히 변경된 부분을 보고 싶다면 <kbd>-d</kbd>을 위의 명령행에
추가함으로써 차이점을 볼 수 있습니다.</p>

<P> 여러분이 오래된 뉴스 항목과 같은 곳에서 번역 파일이 없다는 경고를 무시하고
싶다면, 여러분이 경고를 감추고 싶은 디렉터리에 <code>.transignore</code> 파일을
생성할 수 있습니다. 그리고 여러분이 번역하지 않을 파일의 이름을 한 줄에 하나씩
써서 목록을 만들면 되죠.

<p>메일링 리스트 설명과 관련된 번역을 추적하기 위한 간단한 스크립트도 있습니다.
관련된 문서를 보려면 <code>check_desc_trans.pl</code> 스크립트의 설명을 읽어주세요.
</p>
