#use wml::debian::translation-check translation="f797e8b952b201d953c4e7de221fd635808136e4" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag pagetitle>데비안 9 업데이트: 9.9 나옴</define-tag>
<define-tag release_date>2019-04-27</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 데비안 <release> (코드명 <q><codename></q>)의 9번째 업데이트를 알리게 되어 기쁩니다.
이 포인트 릴리스는 주로 보안 이슈에 대한 수정을 더했으며, 보안 문제를 위한 조정을 따릅니다.
보안 권고는 이미 따로 알려졌고 가능한 곳에서 참조됩니다.</p>

<p>포인트 릴리스는 새로운 버전의 데비안 <release>를 구성하는 것이 아니라 포함 된 일부 패키지만 업데이트한다는 점에 유의하세요.
옛 <q><codename></q> 미디어를 던져버릴 필요는 없습니다.
설치 후 패키지를 최신 데비안 미러를 써서 현재 버전으로 업그레이드 할 수 있습니다.</p>

<p>security.debian.org으로부터의 업데이트를 자주 설치한 사람은 많은 패키지를 업데이트할 필요 없을 것이며,
대부분의 그런 업데이트는 포인트 릴리스에 포함되었습니다.</p>

<p>새 설치 이미지는 정규 위치에서 곧 가능할 겁니다.</p>

<p>기존 설치를 이 버전으로 업그레이드하려면 데비안의 많은 HTTP 미러 중 하나에서 패키지 관리 시스템을 가리켜서 할 수 있습니다.
미러의 완전한 목록은 아래에서 가능:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<p>이 포인트 릴리스의 특별한 경우로 <q>apt-get</q> 도구를 사용하여 업그레이드를 수행하는 경우 <q>dist-upgrade</q> 명령을 사용하여 최신 커널 패키지로 업데이트해야합니다.
<q>apt</q> 및 <q>aptitude</q> 같은 다른 도구를 사용하는 사용자는 <q>upgrade</q> 명령을 사용해야 할겁니다.</p>

<h2>여러가지 버그 고침</h2>

<p>이 안정 업데이트는 몇 가지 중요한 수정 사항을 다음 패키지에 더합니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>까닭</th></tr>
<correction audiofile "Fix denial of service [CVE-2018-13440] and buffer overflow issues [CVE-2018-17095]">
<correction base-files "Update for the point release">
<correction bwa "Fix buffer overflow [CVE-2019-10269]">
<correction ca-certificates-java "Fix bashisms in postinst and jks-keystore">
<correction cernlib "Apply optimization flag -O to Fortran modules instead of -O2 which generates broken code; fix build failure on arm64 by disabling PIE for Fortran executables">
<correction choose-mirror "Update included mirror list">
<correction chrony "Fix logging of measurements and statistics, and stopping of chronyd, on some platforms when seccomp filtering is enabled">
<correction ckermit "Drop OpenSSL version check">
<correction clamav "Fix out-of-bounds heap access when scanning PDF documents [CVE-2019-1787], PE files packed using Aspack [CVE-2019-1789] or OLE2 files [CVE-2019-1788]">
<correction dansguardian "Add <q>missingok</q> to logrotate configuration">
<correction debian-installer "Rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-security-support "Update support statuses">
<correction diffoscope "Fix tests to work with Ghostscript 9.26">
<correction dns-root-data "Update root data to 2019031302">
<correction dnsruby "Add new root key (KSK-2017); ruby 2.3.0 deprecates TimeoutError, use Timeout::Error">
<correction dpdk "New upstream stable release">
<correction edk2 "Fix buffer overflow in BlockIo service [CVE-2018-12180]; DNS: Check received packet size before using [CVE-2018-12178]; fix stack overflow with corrupted BMP [CVE-2018-12181]">
<correction firmware-nonfree "atheros / iwlwifi: update BlueTooth firmware [CVE-2018-5383]">
<correction flatpak "Reject all ioctls that the kernel will interpret as TIOCSTI [CVE-2019-10063]">
<correction geant321 "Rebuild against cernlib with fixed Fortran optmisations">
<correction gnome-chemistry-utils "Stop building the obsolete gcu-plugin package">
<correction gocode "gocode-auto-complete-el: Promote auto-complete-el to Pre-Depends to ensure successful upgrades">
<correction gpac "Fix buffer overflows [CVE-2018-7752 CVE-2018-20762], heap overflows [CVE-2018-13005 CVE-2018-13006 CVE-2018-20761], out-of-bounds writes [CVE-2018-20760 CVE-2018-20763]">
<correction icedtea-web "Stop building the browser plugin, no longer works with Firefox 60">
<correction igraph "Fix a crash when loading malformed GraphML files [CVE-2018-20349]">
<correction jabref "Fix XML External Entity attack [CVE-2018-1000652]">
<correction java-common "Remove the default-java-plugin package, as the icedtea-web Xul plugin is being removed">
<correction jquery "Prevent Object.prototype pollution [CVE-2019-11358]">
<correction kauth "Fix insecure handling of arguments in helpers [CVE-2019-7443]">
<correction libdate-holidays-de-perl "Add March 8th (from 2019 onwards) and May 8th (2020 only) as public holidays (Berlin only)">
<correction libdatetime-timezone-perl "Update included data">
<correction libreoffice "Introduce next Japanese gengou era 'Reiwa'; make -core conflict against openjdk-8-jre-headless (= 8u181-b13-2~deb9u1), which had a broken ClassPathURLCheck">
<correction linux "New upstream stable version">
<correction linux-latest "Update for -9 kernel ABI">
<correction mariadb-10.1 "New upstream stable version">
<correction mclibs "Rebuild against cernlib with fixed Fortran optmisations">
<correction ncmpc "Fix NULL pointer dereference [CVE-2018-9240]">
<correction node-superagent "Fix ZIP bomb attacks [CVE-2017-16129]; fix syntax error">
<correction nvidia-graphics-drivers "New upstream stable release [CVE-2018-6260]">
<correction nvidia-settings "New upstream stable release">
<correction obs-build "Do not allow writing to files in the host system [CVE-2017-14804]">
<correction paw "Rebuild against cernlib with fixed Fortran optmisations">
<correction perlbrew "Allow HTTPS CPAN URLs">
<correction postfix "New upstream stable release">
<correction postgresql-9.6 "New upstream stable release">
<correction psk31lx "Make version sort correctly to avoid potential upgrade issues">
<correction publicsuffix "Update included data">
<correction pyca "Add <q>missingok</q> to logrotate configuration">
<correction python-certbot "Revert to debhelper compat 9, to ensure systemd timers are correctly started">
<correction python-cryptography "Remove BIO_callback_ctrl: The prototype differs with the OpenSSL's definition of it after it was changed (fixed) within OpenSSL">
<correction python-django-casclient "Apply django 1.10 middleware fix; python(3)-django-casclient: fix missing dependencies on python(3)-django">
<correction python-mode "Remove support for xemacs21">
<correction python-pip "Properly catch requests' HTTPError in index.py">
<correction python-pykmip "Fix potential denial of service issue [CVE-2018-1000872]">
<correction r-cran-igraph "Fix denial of service via crafted object [CVE-2018-20349]">
<correction rails "Fix information disclosure issues [CVE-2018-16476 CVE-2019-5418], denial of service issue [CVE-2019-5419]">
<correction rsync "Several security fixes for zlib [CVE-2016-9840 CVE-2016-9841 CVE-2016-9842 CVE-2016-9843]">
<correction ruby-i18n "Prevent a remote denial-of-service vulnerability [CVE-2014-10077]">
<correction ruby2.3 "Fix FTBFS">
<correction runc "Fix root privilege escalation vulnerability [CVE-2019-5736]">
<correction systemd "journald: fix assertion failure on journal_file_link_data; tmpfiles: fix <q>e</q> to support shell style globs; mount-util: accept that name_to_handle_at() might fail with EPERM; automount: ack automount requests even when already mounted [CVE-2018-1049]; fix potential root privilege escalation [CVE-2018-15686]">
<correction twitter-bootstrap3 "Fix cross site scripting issue in tooltips or popovers [CVE-2019-8331]">
<correction tzdata "New upstream release">
<correction unzip "Fix buffer overflow in password protected ZIP archives [CVE-2018-1000035]">
<correction vcftools "Fix information disclosure [CVE-2018-11099] and denial of service [CVE-2018-11129 CVE-2018-11130] via crafted files">
<correction vips "Fix NULL function pointer dereference [CVE-2018-7998], uninitialised memory access [CVE-2019-6976]">
<correction waagent "New upstream release, with many Azure fixes [CVE-2019-0804]">
<correction yorick-av "Rescale frame timestamps; set VBV buffer size for MPEG1/2 files">
<correction zziplib "Fix invalid memory access [CVE-2018-6381], bus error [CVE-2018-6540], out-of-bounds read [CVE-2018-7725], crash via crafted zip file [CVE-2018-7726], memory leak [CVE-2018-16548]; reject ZIP file if the size of the central directory and/or the offset of start of central directory point beyond the end of the ZIP file [CVE-2018-6484, CVE-2018-6541, CVE-2018-6869]">
</table>

<h2>보안 업데이트</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>권고 ID</th>  <th>패키지</th></tr>
<dsa 2018 4259 ruby2.3>
<dsa 2018 4332 ruby2.3>
<dsa 2018 4341 mariadb-10.1>
<dsa 2019 4373 coturn>
<dsa 2019 4374 qtbase-opensource-src>
<dsa 2019 4377 rssh>
<dsa 2019 4385 dovecot>
<dsa 2019 4387 openssh>
<dsa 2019 4388 mosquitto>
<dsa 2019 4389 libu2f-host>
<dsa 2019 4390 flatpak>
<dsa 2019 4391 firefox-esr>
<dsa 2019 4392 thunderbird>
<dsa 2019 4393 systemd>
<dsa 2019 4394 rdesktop>
<dsa 2019 4396 ansible>
<dsa 2019 4397 ldb>
<dsa 2019 4398 php7.0>
<dsa 2019 4399 ikiwiki>
<dsa 2019 4400 openssl1.0>
<dsa 2019 4401 wordpress>
<dsa 2019 4402 mumble>
<dsa 2019 4403 php7.0>
<dsa 2019 4405 openjpeg2>
<dsa 2019 4406 waagent>
<dsa 2019 4407 xmltooling>
<dsa 2019 4408 liblivemedia>
<dsa 2019 4409 neutron>
<dsa 2019 4410 openjdk-8>
<dsa 2019 4411 firefox-esr>
<dsa 2019 4412 drupal7>
<dsa 2019 4413 ntfs-3g>
<dsa 2019 4414 libapache2-mod-auth-mellon>
<dsa 2019 4415 passenger>
<dsa 2019 4416 wireshark>
<dsa 2019 4417 firefox-esr>
<dsa 2019 4418 dovecot>
<dsa 2019 4419 twig>
<dsa 2019 4420 thunderbird>
<dsa 2019 4422 apache2>
<dsa 2019 4423 putty>
<dsa 2019 4424 pdns>
<dsa 2019 4425 wget>
<dsa 2019 4426 tryton-server>
<dsa 2019 4427 samba>
<dsa 2019 4428 systemd>
<dsa 2019 4429 spip>
<dsa 2019 4430 wpa>
<dsa 2019 4431 libssh2>
<dsa 2019 4432 ghostscript>
<dsa 2019 4433 ruby2.3>
<dsa 2019 4434 drupal7>
</table>


<h2>없어진 패키지</h2>

<p>The following packages were removed due to circumstances beyond our control:</p>

<table border=0>
<tr><th>패키지</th>               <th>까닭</th></tr>
<correction gcontactsync "Incompatible with newer firefox-esr versions">
<correction google-tasks-sync "Incompatible with newer firefox-esr versions">
<correction mozilla-gnome-kerying "Incompatible with newer firefox-esr versions">
<correction tbdialout "Incompatible with newer thunderbird versions">
<correction timeline "Incompatible with newer thunderbird versions">

</table>

<h2>데비안 설치관리자</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URL</h2>

<p>이 리비전에서 바뀐 패키지의 완전한 목록:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정 배포:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>안정 배포에 대한 제안된 업데이트:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>안정 배포 정보 (릴리스 노트, 정오표 등.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>보안 알림과 정보:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안은</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>연락 정보</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>

