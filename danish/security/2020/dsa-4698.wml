#use wml::debian::translation-check translation="a871b5fd7d3849d5e29ce5a2e7e4dd60e8408d58" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2182">CVE-2019-2182</a>

    <p>Hanjun Guo og Lei Li rapporterede om en kapløbstilstand i arm64-koden til 
    håndtering af virtuel hukommelse, hvilket kunne føre til en 
    informationsafsløring, lammelsesangreb (nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5108">CVE-2019-5108</a>

    <p>Mitchell Frank fra Cisco opdagede at når IEEE 802.11-stakken (WiFi) blev 
    anvendt i AP-tilstand med roaming, blev der udløst roaming for en nyligt 
    tilknyttet station, før stationen var blevet autentificeret.  En angriber 
    indenfor rækkevidde af AP'et, kunne udnytte fejlen til at forårsage et 
    lammelsesangreb, enten ved at fylde en switching-tabel op eller ved at 
    viderestille trafik væk fra andre stationer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19319">CVE-2019-19319</a>

    <p>Jungyeon opdagede at et fabrikeret filsystem kunne medføre at 
    ext4-implementeringen deallokerede eller renallokerede journalblokke.  En 
    bruger med tilladelse til at mount'e filsystemer, kunne udnytte fejlen til
    at forårsage et lammelsesangreb (nedbrud) eller muligvis 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19462">CVE-2019-19462</a>

    <p>Værktøjet syzbot fandt en manglende fejlkontrol i 
    <q>relay</q>-biblioteket, der anvendes til at implementere forskellige filer 
    under debugfs.  En lokal bruger, med rettigheder til at tilgå debugfs, kunne 
    udnytte fejlen til at forårsage et lammelsesangreb (nedbrud) eller muligvis 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19768">CVE-2019-19768</a>

    <p>Tristan Madani rapporterede om en kapløbstilstand i debugfaciliteten 
    blktrace, hvilket kunne medføre anvendelse efter frigivelse.  En lokal 
    bruger, som er i stand til at udløse fjernelse af blokenheder, kunne 
    muligvis udnytte fejlen til at forårsage et lammelsesangreb (nedbrud) eller 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20806">CVE-2019-20806</a>

    <p>En potentiel nullpointerdereference blev opdaget i mediedriveren tw5864. 
    Sikkerhedspåvirkningen er uklar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20811">CVE-2019-20811</a>

    <p>Hulk Robot-værktøjet fandt en reference-optællingsfejl i en fejlsti i 
    network-undersystemet.  Sikkerhedspåvirkningen er uklar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

    <p>Efterforskere ved VU Amsterdam opdagede at på nogle Intel-CPU'er, som 
    understøtter RDRAND- og RDSEED-instruktioner, kan en del af tilfældige 
    værdier genereret af disse instruktioner blive anvendt i en senere 
    spekulativ udførelse på en vilkårlig kerne i den samme fysiske CPU.  
    Afhængigt af hvordan disse instruktioner anvendes af applikationer, kunne en
    lokal bruger eller VM-gæst udnytte fejlen til at få adgang til følsomme 
    oplysninger, så som kryptografiske nøgler fra andre brugere eller VM'er.</p>

    <p>Sårbarheden kan afhjælpes med en microcode-opdatering, enten som en del 
    af systemfirmware (BIOS) eller ved hjælp af pakken intel-microcode i Debians 
    arkivsektion non-free.  I forbindelse med denne opdatering rapporterer vi kun 
    om sårbarheden og mulighed for at deaktivere afhjælpelsen, hvis den ikke er 
    nødvendig.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

    <p>Paulo Bonzini opdagede at KVM-implementeringen for Intel-processorer ikke 
    på korrekt vis håndterede instruktionsemulering for L2-gæster, når indlejret 
    virtualisering er aktiveret.  Dermed kunne det være muligt for en L2-gæst at 
    forårsage rettighedsforøgelse, lammelsesangreb eller informationslækager i 
    L1-gæsten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8428">CVE-2020-8428</a>

    <p>Al Viro opdagede en potentiel anvendelse efter frigivelse i 
    filesystem-core (vfs).  En lokal bruger kunne udnytte fejlen til at 
    forårsage et lammelsesangreb (nedbrud) eller muligvis få fat i følsomme 
    oplysninger fra kernen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8647">CVE-2020-8647</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-8649">CVE-2020-8649</a>

    <p>Hulk Robot-værktøjet fandt en potentiel MMIO-adgang udenfor grænserne i 
    vgacon-driveren.  En lokal bruger med rettigheder til at tilgå en virtuel 
    terminal (/dev/tty1 osv.) på et system, der anvender vgacon-driveren, kunne 
    udnytte fejlen til at forårsage et lammelsesangreb (nedbrud eller 
    hukommelseskorruption) eller muligvis rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8648">CVE-2020-8648</a>

    <p>Værktøjet syzbot fandt en kapløbstilstand i virtual terminal-driveren, 
    hvilken kunne medføre anvendelse efter frigivelse.  En lokal bruger med 
    rettigheder til at tilgå en virtuel terminal, kunne anvende fejlen til at 
    forårsage et lammelsesangreb (nedbrud eller hukommelseskorruption) eller 
    muligvis rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9383">CVE-2020-9383</a>

    <p>Jordy Zomer rapporterede om en ukorrekt intervalkontrol i floppydriveren, 
    hvilken kunne føre til statisk adgang udenfor grænserne.  En lokal bruger 
    med rettigheder til at tilgå et diskettedrev, kunne udnytte fejlen til at 
    forårsage et lammelsesangreb (nedbrud eller hukommelseskorruption) eller 
    muligvis rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10711">CVE-2020-10711</a>

    <p>Matthew Sheets rapporterede om problemer med NULL-pointerdereferencer i 
    undersystemet SELinux mens der modtages CIPSO-pakker med null-kategori.  En 
    fjernangriber kunne drage nytte af fejlen til at forårsage et 
    lammelsesangreb (nedbrud).  Bemærk at dette problem ikke påvirker de binære 
    pakker, som distribueres i Debian, da CONFIG_NETLABEL ikke er 
    aktiveret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10732">CVE-2020-10732</a>

    <p>En informationslækage af privat kernehukommelse til brugerrummet blev 
    fundet i kernens implementering af core dumping-brugerrumprocesser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10751">CVE-2020-10751</a>

    <p>Dmitry Vyukov rapporterede at undersystemet SELinux ikke på korrekt vis 
    håndterede validering af flere meddelelser, hvilket kunne gøre det muligt 
    for en priviligeret bruger at omgå SELinux' netlinkbegrænsninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10757">CVE-2020-10757</a>

    <p>Fan Yang rapporterede om en fejl i den måde, mremap håndterede DAX-hugepages, 
    hvilket gjorde det muligt for en lokal bruger at forøge sine 
    rettigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

    <p>Man opdagede at vhost_net-driveren ikke på korrekt vis validerede typen 
    af sockets opsat som backends.  En lokal bruger med rettigheder til at tilgå 
    /dev/vhost-net, kunne udnytte fejlen til at forårsage stakkorruption gennem 
    fabrikerede systemkald, medførende lammelsesangreb (nedbrud) eller muligvis 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11494">CVE-2020-11494</a>

    <p>Man opdagede at netværksdriveren slcan (serial line CAN) ikke 
    fuldstændigt initialiserede CAN-headere for modtagne pakker, medførende 
    en informationslækage fra kernen til brugerrummet eller over 
    CAN-netværket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

    <p>Entropy Moe rapporterede at delt hukommelse-filsystemet (tmpfs) ikke på 
    korrekt vis håndterede en <q>mpol</q>-mountvalgmulighed, som angiver en tom 
    nodeliste, førende til en stakbaseret skrivning udenfor grænserne.  Hvis 
    brugernavnerum er aktiveret, kunne en lokal bruger udnytte fejltn til at 
    forårsage et lammelsesangreb (nedbrud) eller muligvis 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11608">CVE-2020-11608</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-11609">CVE-2020-11609</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-11668">CVE-2020-11668</a>

    <p>Man opdagede at mediedriverne ov519, stv06xx og xirlink_cit ikke på 
    korrekt vis validerede USB-enhedsdescriptors.  En fysisk tilstedeværende 
    bruger, med en særligt fremstillet USB-enhed, kunne udnytte fejlen til at 
    forårsage et lammelsesangreb (nedbrud) eller muligvis 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12114">CVE-2020-12114</a>

    <p>Piotr Krysiuk opdagede en kapløbstilstand mellem umount- og 
    pivot_root-handlinger i filesystem-core (vfs).  En lokal bruger med en 
    CAP_SYS_ADMIN-kapabilitet i ethvert brugernavnerum, kunne udnytte fejlen til 
    at forårsage et lammelsesangreb (nedbrud).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12464">CVE-2020-12464</a>

    <p>Kyungtae Kim rapporterede om en kapløbstilstand i USB-core, hvilken kunne 
    medføre anvendelse efter frigivelse.  Det er uklart hvordan denne fejl kan 
    udnyttes, men det medføre et lammelsesangreb (nedbrud eller 
    hukommelseskorruption) eller rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12652">CVE-2020-12652</a>

    <p>Tom Hatskevich rapporterede en fejl i mptfusion-storagedriverne.  En 
    ioctl-handler hentede et parameter to gange fra brugerhukommelsen, hvilket 
    skabte en kapløbstilstand, som kunne medføre ukorrekt låsning af interne 
    datastrukturer.  En lokal bruger med rettigheder til at tilgå /dev/mptctl, 
    kunne udnytte fejlen til at forårsage et lammelsesangreb (nedbrud eller 
    hukommelseskorruption) eller rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12653">CVE-2020-12653</a>

    <p>Man opdagede at WiFi-driveren mwifiex ikke validerede scanforespørgsler 
    tilstrækkeligt, medførende et potentielt heap-bufferoverløb.  En lokal 
    bruger med CAP_NET_ADMIN-kapabiliteten, kunne udnytte fejlen til at 
    forårsage et lammelsesangreb (nedbrud eller hukommelseskorruption) eller 
    muligvis rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12654">CVE-2020-12654</a>

    <p>Man opdagede at WiFi-driveren mwifiex ikke validerede WMM-parametre 
    modtaget fra et accesspoint (AP), medførende et potentielt 
    heap-bufferoverløb.  Et ondsindet AP kunne udnytte fejlen til at forårsage 
    et lammelsesangreb (nedbrud eller hukommelseskorruption) eller muligvis 
    udførelse af kode på et sårbart system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12770">CVE-2020-12770</a>

    <p>Man opdagede at sg-driveren (SCSI generic) ikke på korrekt vis frigav 
    interne ressourcer i en bestemt fejlsituation.  En lokal bruger med 
    rettigheder til at tilgå en sg-enhed, kunne muligvis anvende fejlen til 
    at forårsage et lammelsesangreb (udmattelse af ressourcer).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13143">CVE-2020-13143</a>

    <p>Kyungtae Kim rapporterede om en potentiel heap-skrivning udenfor 
    grænserne i USB-gadget-undersystemet.  En lokal bruger med rettigheder til 
    at skrive til gadgetopsætningsfilsystemet, kunne anvende fejlen til at 
    forårsage et lammelsesangreb (nedbrud eller hukommelseskorruption) eller 
    potentielt rettighedsforøgelse.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet i 
version 4.9.210-1+deb9u1.  Versionen retter også nogle relaterede fejl, som ikke 
har deres egne CVE-ID'er, samt en regression i macvlan-driveren, opstået i 
forbindelse med den tidligere punktopdatering (fejl nummer 952660).</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4698.data"
