#use wml::debian::template title="Privacy Policy" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="caced2d06b9224deef495d02a009f78a1cf8acd8"

## Translators may want to add a note stating that the translation 
## is only informative and has no legal value, and people
## should look at the original English for that. 
## Some languages have already put such a note in the
## translations of /trademark and /license, for example.

<p>Dette er en oversættelse af det <a href="privacy.en.html">originale</a>
engelsksprogede dokument.  Oversættelsen er kun til information.  Originalen er 
det eneste gældende dokument.</p>

<p><a href="https://www.debian.org/">Debian-projektet</a> er en frivillig 
sammenslutning af enkeltpersoner, der har det fælles mål at oprette et frit 
tilgængeligt styresystem, herefter kaldet Debian.</p>

<p>Der er intet krav om, at nogen der ønsker at anvende Debian, skal levere 
nogen form for personlige oplysninger til projektet; Debian kan downloades frit, 
uden registrering eller anden form for identifikation, fra både officielle 
filspejle varetaget af projektet selv og fra talrige tredjeparter.</p>

<p>Forskellige andre interaktioner med Debian-projektet, involverer dog 
indsamling af personlige oplysninger.  Det sker primært i form af navne og 
mailadresser i mails modtaget af projektet; alle Debians postlister arkiveres 
offentligt, hvilket også gælder fejlsporingssystemet.  Det er i overensstemmelse 
med vores <a href="https://www.debian.org/social_contract">sociale kontrakt</a>, 
i særdeleshed vores erklæring om at ville give noget tilbage til fri 
software-fællesskabet (#2), og fordi vi ikke skjuler vores problemer (#3).  Vi 
foretager ikke yderligere behandling på nogen form for oplysninger, som vi er i 
besiddelse af, men der er tilfælde hvor det automatisk deles med tredjeparter 
(så som mail til lister, eller interaktivt med fejlsporingssystemet).</p>

<p>Listen herunder kategoriserer de forskellige tjenester, som varetages af 
projektet, oplysninger der anvendes af disse tjenester og årsagerne til at det 
er nødvendigt.</p>

<p>Bemærk at hosts og tjenester under domænet <strong>debian.net</strong>, ikke 
indgår i det officielle Debian-projekt; de varetages af enkeltpersoner, der har 
en tilkytning til projektet, snarere end selv at repræsentere projektet.  
Spørgsmål om præcis hvilke data disse tjenster opbevarer, skal sendes til 
tjenesternes ejere, og ikke til Debian-projektet.</p>

<p>Spørgsmål om disse tjenester bør først og fremmest rettes til de respektive 
ejere.  Hvis det er uklart hvem en ejer er, kan man kontakte 
databeskyttelsesholdet på <a href="mailto:data-protection@debian.org">\
data-protection@debian.org</a>, som vil forsøge at sende forespørgslen videre 
til de rette ansvarlige.</p>


<h2>Bidragydere (<a href="https://contributors.debian.org/">contributors.debian.org</a>)</h2>

<p>Webstedet Debian Contributors (bidragydere til Debian), indeholder en 
opsamling af data om hvor nogen har bidraget til Debian-projektet, uanset om det 
er ved at indsende en fejlrapport, foretage en upload til arkivet, skrive et 
indlæg på en postliste eller forskellige andre interaktioner med projektet.  
Oplysningerne kommer fra de pågældende tjenster (detaljerede identifikatorer så 
som loginnavn og tidspunktet for seneste bidrag), og giver et enkelt 
referencested, hvor man kan se, hvor projektet opbevarer oplysninger om en 
enkeltperson.</p>


<h2>Arkivet (<a href="https://ftp.debian.org/debian/">ftp.debian.org</a>)</h2>

<p>Debians primære distributionsmetode, er gennem dets offentlige arkivnetværk. 
Arkivet består af alle de binære pakker og deres tilknyttede kildekode, der 
indeholder personlige oplysninger i form af navne og mailadresser, opbevaret som 
en del af ændringslogninger, ophavsretoplysninger og generel dokumentation.  
Størstedelen af oplysningerne leveres af opstrømssoftwareforfatternes 
distribuerede kildekode, hvortil Debian tilføjer yderligere oplysninger, for at 
kunne spore forfatterskab og ophavsret, for at sikre at licenserne er 
dokumenteret korrekt, og at Debians retningslinjer for fri software 
overholdes.</p>


<h2>BTS, fejlsporingssystemet (<a href="https://bugs.debian.org/">bugs.debian.org</a>)</h2>

<p>Der interageres med fejlsporingssystemet (BTS) gennem mail, og alle mails 
modtaget i forbindelse med en fejl, gemmes som en del af den fejls historik.  
For at projektet effektivt kan håndtere problemer, fundet i distributionen, og 
for at gøre det muligt for brugerne at se uddybende oplysninger om disse 
problemer, samt hvorvidt der er en tilgængelig rettelse eller omgåelse, er det 
komplette fejlsporingssystem åbent for opslag.  Derfor bliver alle oplysninger, 
herunder navne og mailadresser som en del af mailheadere, sendt til 
fejlsporingssystemet, gemt og er offentligt tilgængelige.</p>


<h2>DebConf (<a href="https://www.debconf.org/">debconf.org</a>)</h2>

<p>DebConfs registreringsstruktur gemme oplysninger om konferencedeltagere.  Det 
er nødvendigt for at afklare om en person er berettiget til et rejsestipendium, 
er tilknyttet projektet, og for at kontakte deltagere med de rette oplysninger. 
Oplysningerne kan også blive delt med underleverandører til konferencen, 
eksempelvis deles med indkvarteringsstedet navne og deltagelsesdatoer på 
konferencedeltagerne, der benytter sig af konferencens 
indkvarteringsmulighed.</p>


<h2>Udvikler-LDAP (<a href="https://db.debian.org">db.debian.org</a>)</h2>

<p>I projektets LDAP-infrastruktur gemmes oplysninger om projektbidragydere 
(udviklere og andre med gæstekonti), der har kontoadgang til maskiner indenfor 
Debians infrastruktur.  Der gemmes primært navne, brugernavne og 
autentifikationsoplysninger.  Bidragydere kan dog også valgfrit afgive 
oplysninger så som køn, chatsystem (IRC/XMPP), land, adresse og telefonnnummer, 
samt en besked hvis de er på ferie.</p>

<p>Navn, brugernavn og nogle af de frivilligt afgivne oplysninger, er frit 
tilgængelige gennem webbrugergrænsefladen eller LDAP-søgning.  Yderligere 
oplysninger deles kun med andre personer, der har kontoadgang til Debians 
infrastruktur, og har til formål at stille et centraliseret sted til rådighed, 
hvor projektmedlemmer kan udveksle sådanne kontaktoplysninger.  Det opsamles 
ikke på noget tidspunkt eksplicit, og kan altid fjernes ved at logge på 
webbrugergrænsefladen til db.debian.org eller ved at sende en signeret mail 
til mailgrænsefladen.  Se <a href="https://db.debian.org/">\
https://db.debian.org/</a> og
<a href="https://db.debian.org/doc-general.html">\
https://db.debian.org/doc-general.html</a> for flere oplysninger.</p>


<h2>GitLab (<a href="https://salsa.debian.org/">salsa.debian.org</a>)</h2>

<p>salsa.debian.org indeholder en instans af 
<a href="https://about.gitlab.com/">GitLab</a> DevOps-værktøjet til håndtering 
af livsforløb.  Det anvendes især af projektet til at gøre det muligt for 
projektbidragydere, at opbevare softwarearkiver ved hjælp af Git, og opmuntrer 
til samarbejde mellem bidragydere.  Som følge heraf kræver det forskellige 
personlige oplysninger, at håndtere konti.  For projektmedlemmers vedkommende 
er det knyttet til Debians central LDAP-system, men gæster kan også oprette en 
konto, og skal oplyse navn og mailadresse, for at kontoen kan opsættes og 
benyttes.</p>

<p>Salsas primære formål er at håndtere vores git-historik.  Se nedenfor.</p>


<h2><a name="git">git</a></h2>

<p>Debian vedligeholder adskillige git-servere, der indeholder kildekode og 
lignende materialer, samt deres revisions- og bidraghistorikker.  Individuelle 
Debian-bidragydere har typisk også oplysninger, som opbevares i git.</p>

<p>Git-historikken indeholder navne og mailadresser på bidragydere (herunder 
fejlrapportører, forfattere, reviwere, osv.).  Oplysningerne befinder sig 
permanent i git, i en historik der kun kan tilføjes til.  Vi benytter kun 
tilføjelse-historik, fordi den har vigtige integritetsegenskaber, blandt 
andre betydelige beskyttelser mod ikke-sporede ændringer.  Oplysningerne 
opbevares uendeligt, så vi altid er i stand til at verificere bidrags ophavsret 
og anden juridisk status, og så vi af softwareitegritetsårsager altid kan 
identificere ophavet.</p>

<p>Git-systemets kun tilføjelser-natur, medfører at enhver ændring af disse 
commit-oplysninger, når de først er blevet indført i arkivet, vil være ekstremt 
ødelæggende, og i nogle tilfælde umulige (når eksempelvis signerede commits 
benyttes).  Dette undgås derfor, bortset fra i ekstreme situationer.  Hvor det 
er passende, benytter vi git-funktionalitet (fx <code>mailmap</code>) til at 
tilpasse de historiske personlige oplysninger, om end de er bevaret, kan de 
udelades eller korrigeres i forbindelse med præsentation og anvendelse.</p>

<p>Med mindre der er ekstraordinære årsager til at gøre andet, er Debians 
git-historikker, herunder tilknyttede personlige oplysninger om bidragydere, 
fuldstændig offentligt tilgængelige.</p>


<h2>Gobby (<a href="https://gobby.debian.org/">gobby.debian.org</a>)</h2>

<p>Gobby er en onlineteksteditor til samarbejde, der holder styr på bidrag og 
tilsluttede brugere.  Ingen autentifikation er nødvendig for at forbinde sig 
til systemet, og brugerne kan selv finde på et tilfældigt brugernavn.  Om end 
tjenesten ikke gør noget forsøg på at holde styr på hvem, der ejer hvilke 
brugernavne, skal man være opmærksom på at det kan vise sig at være muligt at 
knytte brugernavne til personer, baseret på udbredt brug af det pågældende 
brugernavn eller det indhold, man bidrager med til samarbejdsdokumenter i 
systemet.</p>


<h2>Postlister (<a href="https://lists.debian.org/">lists.debian.org</a>)</h2>

<p>Postlister er Debian-projektets primære kommunikationsmetode.  Næsten alle 
postlister, som er tilknyttet projektet, er åbne, og dermed tilgængelige for 
alle og enhver, til både læsning og skrivning.  Alle lister arkiveres også; hvad 
angår offentlige lister, er de tilgængelige via en webgrænseflade.  Det opfylder 
projektets egen forpligtelse til åbenhed, og hjælper vores brugere og udviklere 
med at forstå, hvad der foregår i projektet, eller med at forstå de historiske 
årsager til visse aspekter ved projekter.  På grund af den måde e-mail fungerer 
på, kan disse arkiver dermed potentielt indeholde personlige oplysninger, så som 
navne og mailadresser.</p>


<h2>Websted for nye medlemmer (<a href="https://nm.debian.org/">nm.debian.org</a>)</h2>

<p>Bidragydere til Debian-projektet, som ønsker at formalisere deres 
involvering, kan beslutte at deltage i nyt medlem-processen.  Det giver dem 
mulighed for at uploade deres egne pakker (som Debian-vedligeholder) eller at 
blive fuldgyldigt stemmeberettiget medlem af projektet, med kontorettigheder 
(Debian-udvikler, med mulighed for at uploade eller ikke at uploade).  Som en 
del af processen, indsamles forskellige personlige oplysninger, begyndende med 
navn, mailadresse og oplysninger om krypterings-/signeringsnøgle.  Processen med 
at blive fuldgydligt medlem, involverer også korrespondance med en Application 
Manager, der via mail kommunikerer med det nye medlem, for at sikre at 
vedkommende har forstået principperne bag Debian, og har de nødvendige 
færdigheder, til at benytte projektets infrastruktur.  Mailkommunikationen 
arkiveres og er tilgængelig for ansøgeren og Application Managers gennem 
nm.debian.org.  Forskellige oplysninger om igangværende ansøgninger er 
offentligt tilgængelige på webstedet, og giver af hensyn til gennemskueligheden 
alle mulighed for at se status på nyt medlem-processen i projektet.</p>


<h2>Popularity Contest (<a href="https://popcon.debian.org/">popcon.debian.org</a>)</h2>

<p><q>popcon</q> holder styr på hvilke pakker, der er installeret på et 
Debian-system, for at gøre det muligt at indsamle statistik om hvilke pakker, 
der er vidt anvendt og hvilke der ikke længere er i brug.  Der benyttes den 
valgfrie pakke <q>popularity-contest</q> til indsamling af oplysningerne, som 
kræver at man eksplicit tilmelder sig.  Det giver nyttig vejledning om hvor 
udviklerressourcerne kan benyttes, for eksempel når der migreres til nyere 
versioner af biblioteker, og hvor man er nødt til at bruge tid på at ændre 
ældre applikationer.  Hver popcon-instans genererer en tilfældig, unik 128 
bit-id, som anvendes til at holde styr på indsendelser fra den samme 
host.  Der gøres ingen forsøg på at knytte oplysningerne til personer. 
Indsendelser sker via mail eller HTTP, og det er derfor muligt, at der kan ske 
en lækage af personlige oplysninger, i form af den anvendte IP-adresse eller 
gennem mailheadere.  Oplysningerne er kun tilgængelige for Debians 
systemadministratorer og popcon-administratorer; alle sådanne metadata fjernes 
før indsendelser gøres tilgængelige for projektet som helhed.  Brugernes skal 
dog være opmærksomme på, at unikke pakkesignaturer (så som lokalt fremstillede 
pakker eller pakker med få installeringer), kan medføre at nogen kan deducere 
sig frem til at en given maskine tilhører en bestemt person.</p>

<p>De rå indsendelser opbevares i 24 timer, så det er muligt at anvende dem 
igen, hvis der har været problemer med behandlingen.  Anonymiserede 
indsendelser gemmes i maksimalt 20 dage.  Opsummerende rapporter, som ikke 
indeholder personidentificerbare oplysninger, opbevares uendeligt.</p>


<h2>snapshot (<a href="http://snapshot.debian.org/">snapshot.debian.org</a>)</h2>

<p>Snapshot-arkivet indeholder et historisk kig på Debians arkiv (ftp.debian.org 
herover), og giver adgang til gamle pakker, baseret på datoer og versionsnumre.  
I forhold til hovedarkivet er der ingen yderligere oplysninger (dvs. at det kan 
indeholde navne og mailadresser i changelog-filer, ophavsretserklæringer samt i 
anden dokumentation), men kan indeholde pakker, som ikke længere leveres med 
Debians udgivelser.  Det er en nyttig ressource for udviklere og bruger, når man 
prøver at finde regressioner i softwarepakker, eller for at kunne etablere et 
specifikt miljø til at afvikle en bestemt applikation.</p>


<h2>Afstemninger (<a href="https://vote.debian.org/">vote.debian.org</a>)</h2>

<p>Afstemningssystemet (devotee) holder styr på igangværende generelle 
beslutninger og resultaterne af tidligere afstemninger.  I de fleste tilfælde 
betyder det, at når en afstemningsperiode er afsluttet, bliver oplysningerne om 
hvem der stemte (brugernavne og personnavne) og hvordan de stemte, gjort 
offentligt tilgængelige.  Kun projektmedlemmer er stemmeberettigede, der hvor 
devotee anvendes, og systemet holder kun styr på stemmeberettigedes stemmer.</p>


<h2>Wiki (<a href="https://wiki.debian.org/">wiki.debian.org</a>)</h2>

<p>Debians Wiki er en af projektets support- og dokumentationsressourcer, som 
alle kan redigere.  Heri indgår at bidrag spores over tid og er tilknyttet 
brugerkonti i wikien; enhver ændring af en side spores, for at sikre at 
fejlagtige redigeringer kan rulles tilbage, og opdaterede oplysninger let kan 
undersøges.  Sporingen indeholder oplysninger om brugeren, der foretog en given 
ændring, hvilket kan benyttes til at forhindre misbrug, ved at spærre brugeres 
og IP-adressers adgang til at redigere.  Brugerkonti gør det også muligt for 
brugere at tegne abonnement på sider for at overvåge ændringer, eller at se 
samtlige ændringer i hele wikien siden de sidst kiggede derpå.  Generelt er 
brugerkonti navngivet efter personen, der benytter dem, men der sker ingen 
validering af kontonavne, og en bruger kan vælge et hvilket som helst ledigt 
kontonavn.  En mailadresse er krævet, for at gøre det muligt at nulstille 
adgangskoden, og for at give brugeren besked om ændrede sider, som vedkommende 
har tegnet abonnement på.</p>


<h2>Git-serveren dgit (<a href="https://browse.dgit.debian.org/">*.dgit.debian.org</a>)</h2>

<p>Git-serveren dgit er git-arkivet indeholdende (revisionskontrols-) historik
over pakker, der er uploadet til Debian ved hjælp af <code>dgit push</code>, 
som anvendes af <code>dgit clone</code>.  Den indeholder oplysninger, der ligner 
Debians arkiv og Salsas gitlab-instans: kopier af (og tidligere snapshots af) 
de samme oplysninger, men i git-format.  Bortset fra pakker, der gennemgås af 
Debians ftpmastere for første gang, for medtagelse i Debian: alle disse 
git-oplysninger er altid offentligt tilgængelige, og opbevares uendeligt.  
Se <a href="#git">git</a> herover.</p>

<p>Af adgangskontrolhensyn, er der en manuelt vedligeholdt liste over 
Debian-vedligeholderes offentlige ssh-nøgler, hvor de har bedt om at blive 
tilføjet.  Listen er ikke offentligt tilgængelig.  Vi håber at kunne nedlægge 
tjenesten, og i stedet anvende komplette data vedligeholdt af en anden 
Debian-tjeneste.</p>


<h2>Echelon</h2>

<p>Echelon er et system, der anvendes af projektet til at holde styr på 
medlemmernes aktivitet; i særdeleshed overvåges postlisternes og arkivernes 
infrastrukturer, for at finde indlæg og upload til dokumentering af at et 
Debian-medlem er aktivt.  Kun de allerseneste akiviteter gemmes, i medlemmets 
LDAP-registrering.  Det er dermed begrænset til kun at holde styr på 
detaljerede oplysninger om enkeltpersoner, som har konti i Debians 
infrastruktur.  Oplysningerne anvendes til at afgøre om et projektmedlem er 
inaktivt eller er forsvundet, og der dermed kan være operationelle krav om at 
låse deres konti eller på anden måde at begrænse deres adgangsrettigheder, for 
at sikre at Debians systemer holde sikre.</p>


<h2>Servicerelateret logning</h2>

<p>Ud over de hverover eksplicit anførte tjenester, logger Debians infrastruktur 
detaljerede oplysninger om systemtilgang, for at sikre tjenesternes 
tilgængelighed og pålidelighed, og for at muliggøre debugging og 
diagnostificering af opståede problemer.  Logningen indeholder oplysninger om 
sendte/modtagne mails gennem Debians infrastruktur, websideadgangsforespørgsler 
sendt til Debians infrastruktur, samt loginoplysninger vedrørende 
Debian-systemer (eksempelvis SSH-logins til projektmaskiner).  Ingen af disse 
oplysninger anvendes til noget andet formål, end de operationelle krav, og de 
gemmes i 15 dage hvad angår webserverlogninger, 10 dage hvad angår 
mailserverlogninger og i fire uger hvad angår autentifikations- og 
SSH-logninger.</p>
