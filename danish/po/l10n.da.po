msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:53+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Fil"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Pakke"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Point"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Oversætter"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Hold"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Dato"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Status"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Strenge"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Fejl"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, som det tales i <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Ukendt sprog"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Denne side er genereret med data opsamlet den <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr ""
"Før du arbejder på disse filer, skal du forvisse dig om at de er ajourførte!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Sektion: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "Lokaltilpasning"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Liste over sprog"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Placering"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Tips"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Fejl"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "POT-filer"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Tips til oversættere"

#~ msgid "Original templates"
#~ msgstr "Original skabelon"

#~ msgid "Translated templates"
#~ msgstr "Oversatte skabeloner"

#~ msgid "Todo"
#~ msgstr "Skal gøres"
