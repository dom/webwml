#use wml::debian::template title="Debian GNU/kFreeBSD"
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f"
#use wml::debian::toc

<toc-display/>

<p>Debian GNU/kFreeBSD er en tilpasning der består af
<a href="https://www.gnu.org/">GNU-userland</a> med anvendelse af
<a href="https://www.gnu.org/software/libc/">GNU C-biblioteket</a> oven på
<a href="https://www.freebsd.org/">FreeBSD</a>s kerne og kernerelaterede
værktøjer, sammen med det almindelige <a href="https://packages.debian.org/">\
sæt Debian-pakker</a>.</p>

<div class="important">
<p>Denne tilpasning er under udarbejdelse.  Den blev udgivet med Debian 6.0 
(Squeeze) som en teknologisk forsmag og som den første 
ikke-Linux-tilpasning.</p>
</div>


<toc-add-entry name="resources">Ressourcer</toc-add-entry>

<p>Der er flere oplysninger om tilpasningen (herunder OSS) på wikisiden om 
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>.</p>

<h3>Postliste</h3>

<p><a href="https://lists.debian.org/debian-bsd">Postlisten Debian GNU/k*BSD</a>.</p>

<h3>IRC</h3>

<p><a href="irc://irc.debian.org/#debian-kbsd">IRC-kanalen #debian-kbsd</a> (på 
irc.debian.org).</p>


<toc-add-entry name="Development">Udvikling</toc-add-entry>

<p>Da vi anvender Glibc er tilpasningsproblemer meget simple og i de fleste 
tilfælde drejer det sig bare om at kopiere en testsag til <q>k*bsd*-gnu</q> fra 
et andet Glibc-baseret system (som GNU eller GNU/Linux).  Se 
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">\
tilpasningsdokumentet</a> for flere oplysninger.</p>

<p>Se også <a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">\
TODO-filen</a> for flere oplysninger om, hvad der mangler at blive gjort.</p>


<toc-add-entry name="availablehw">Tilgængelig hardware til Debian-udviklere</toc-add-entry>

<p>lemon.debian.net (kfreebsd-amd64) er tilgængelig for Debian-udviklere til 
tilpasningsarbejde.  Se <a href="https://db.debian.org/machines.cgi">\
maskinedatabasen</a> for flere oplysninger om disse maskiner.  Generelt vil man 
kunne anvende to chroot-miljøer:  testing og unstable.  Bemærk at systemet 
ikke systemadministreres af DSA, så <strong>send ikke forespørgsler til 
debian-admin om dem</strong>.  Anvend i stedet 
<email "admin@lemon.debian.net">.</p>
