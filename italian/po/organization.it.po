# Debian Web Site. organization.it.po
# Copyright (C) 2002 Giuseppe Sacco.
# Giuseppe Sacco <eppesuigoccas@libero.it>, 2003.
# Giuseppe Sacco <eppesuig@debian.org>, 2004, 2005.
# Luca Monducci <luca.mo@tiscali.it>, 2006 - 2020.
# 
msgid ""
msgstr ""
"Project-Id-Version: organization.it\n"
"PO-Revision-Date: 2020-06-06 14:52+0200\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "email di delega"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "email di nomina"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegato"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegata"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>lui/suo"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>lei/sua"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegato"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>essi/loro"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "attuale"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "membro"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "manager"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Gestore del rilascio stabile"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "mago"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "presidente"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistente"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "segretario"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "rappresentativo"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "ruolo"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"Nel seguente elenco, <q>attuale</q> è usato per gli incarichi che sono "
"temporanei (per elezione o per nomina e con data di decadenza certa)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Direttori"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:105
msgid "Distribution"
msgstr "Distribuzione"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:237
msgid "Communication and Outreach"
msgstr "Comunicazione e sociale"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:240
msgid "Data Protection team"
msgstr "Team protezione dei dati"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:244
msgid "Publicity team"
msgstr "Team Pubblicità"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:315
msgid "Membership in other organizations"
msgstr "Appartenenza in altre organizzazioni"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:343
msgid "Support and Infrastructure"
msgstr "Supporto e infrastruttura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Leader"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Commissione tecnica"

#: ../../english/intro/organization.data:100
msgid "Secretary"
msgstr "Segretario"

#: ../../english/intro/organization.data:108
msgid "Development Projects"
msgstr "Progetti di sviluppo"

#: ../../english/intro/organization.data:109
msgid "FTP Archives"
msgstr "Archivi FTP"

#: ../../english/intro/organization.data:111
msgid "FTP Masters"
msgstr "FTP Master"

#: ../../english/intro/organization.data:117
msgid "FTP Assistants"
msgstr "Assistenti FTP"

#: ../../english/intro/organization.data:123
msgid "FTP Wizards"
msgstr "Maghi FTP"

#: ../../english/intro/organization.data:127
msgid "Backports"
msgstr "Backport"

#: ../../english/intro/organization.data:129
msgid "Backports Team"
msgstr "Team Backport"

#: ../../english/intro/organization.data:133
msgid "Release Management"
msgstr "Gestione del rilascio"

#: ../../english/intro/organization.data:135
msgid "Release Team"
msgstr "Team di rilascio"

#: ../../english/intro/organization.data:144
msgid "Quality Assurance"
msgstr "Controllo qualità"

#: ../../english/intro/organization.data:145
msgid "Installation System Team"
msgstr "Team per il sistema d'installazione"

#: ../../english/intro/organization.data:146
msgid "Debian Live Team"
msgstr "Team Debian Live"

#: ../../english/intro/organization.data:147
msgid "Release Notes"
msgstr "Note di rilascio"

#: ../../english/intro/organization.data:149
msgid "CD/DVD/USB Images"
msgstr "Immagini di CD/DVD/USB"

#: ../../english/intro/organization.data:151
msgid "Production"
msgstr "Produzione"

#: ../../english/intro/organization.data:158
msgid "Testing"
msgstr "Test"

#: ../../english/intro/organization.data:160
msgid "Cloud Team"
msgstr "Team Cloud"

#: ../../english/intro/organization.data:164
msgid "Autobuilding infrastructure"
msgstr "Infrastruttura di compilazione automatica"

#: ../../english/intro/organization.data:166
msgid "Wanna-build team"
msgstr "Team wanna-build"

#: ../../english/intro/organization.data:173
msgid "Buildd administration"
msgstr "Amministrazione di buildd"

#: ../../english/intro/organization.data:191
msgid "Documentation"
msgstr "Documentazione"

#: ../../english/intro/organization.data:196
msgid "Work-Needing and Prospective Packages list"
msgstr ""
"Elenco dei pacchetti richiesti e di quelli che necessitano di manodopera "
"(WNPP)"

#: ../../english/intro/organization.data:198
msgid "Ports"
msgstr "Port"

#: ../../english/intro/organization.data:228
msgid "Special Configurations"
msgstr "Configurazioni speciali"

#: ../../english/intro/organization.data:230
msgid "Laptops"
msgstr "Portatili"

#: ../../english/intro/organization.data:231
msgid "Firewalls"
msgstr "Firewall"

#: ../../english/intro/organization.data:232
msgid "Embedded systems"
msgstr "Sistemi embedded"

#: ../../english/intro/organization.data:247
msgid "Press Contact"
msgstr "Contatto per la stampa"

#: ../../english/intro/organization.data:249
msgid "Web Pages"
msgstr "Pagine web"

#: ../../english/intro/organization.data:261
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:266
msgid "Outreach"
msgstr "Coninvolgimento in progetti esterni"

#: ../../english/intro/organization.data:271
msgid "Debian Women Project"
msgstr "Progetto Debian Women"

#: ../../english/intro/organization.data:279
msgid "Community"
msgstr "Comunità"

#: ../../english/intro/organization.data:286
msgid "Events"
msgstr "Eventi"

#: ../../english/intro/organization.data:293
msgid "DebConf Committee"
msgstr "Commissione DebConf"

#: ../../english/intro/organization.data:300
msgid "Partner Program"
msgstr "Programma per i partner"

#: ../../english/intro/organization.data:305
msgid "Hardware Donations Coordination"
msgstr "Coordinazione delle donazioni hardware"

#: ../../english/intro/organization.data:321
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:323
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:325
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:327
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:329
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:330
msgid "SchoolForge"
msgstr "SchoolForge"

#: ../../english/intro/organization.data:333
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:336
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:339
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:346
msgid "User support"
msgstr "Supporto agli utenti"

#: ../../english/intro/organization.data:413
msgid "Bug Tracking System"
msgstr "Sistema di tracciamento dei bug (BTS)"

#: ../../english/intro/organization.data:418
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Amministrazione e archivi delle liste di messaggi"

#: ../../english/intro/organization.data:427
msgid "New Members Front Desk"
msgstr "Accoglienza dei nuovi membri"

#: ../../english/intro/organization.data:433
msgid "Debian Account Managers"
msgstr "Gestori degli account Debian"

#: ../../english/intro/organization.data:437
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Per inviare un messaggio privato a tutti i Gestori degli account Debian, "
"usare la chiave GPG 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:438
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Gestori del Keyring (PGP e GPG)"

#: ../../english/intro/organization.data:442
msgid "Security Team"
msgstr "Team della sicurezza"

#: ../../english/intro/organization.data:453
msgid "Consultants Page"
msgstr "Pagina dei consulenti"

#: ../../english/intro/organization.data:458
msgid "CD Vendors Page"
msgstr "Pagina dei rivenditori di CD"

#: ../../english/intro/organization.data:461
msgid "Policy"
msgstr "Regolamento"

#: ../../english/intro/organization.data:464
msgid "System Administration"
msgstr "Amministrazione del sistema"

#: ../../english/intro/organization.data:465
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Questo è l'indirizzo da utilizzare quando si incontrano problemi in una "
"delle macchine Debian, ivi inclusi i problemi relativi a password o a "
"pacchetti non installati."

#: ../../english/intro/organization.data:475
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Se trovi un problema hardware su una macchina Debian, vedi la pagina delle "
"<a href=\"https://db.debian.org/machines.cgi\">macchine Debian</a>, dovrebbe "
"contenere informazioni sugli amministratori divisi per macchine."

#: ../../english/intro/organization.data:476
msgid "LDAP Developer Directory Administrator"
msgstr "Amministratore dell'elenco LDAP degli sviluppatori"

#: ../../english/intro/organization.data:477
msgid "Mirrors"
msgstr "Mirror"

#: ../../english/intro/organization.data:484
msgid "DNS Maintainer"
msgstr "Gestori del DNS"

#: ../../english/intro/organization.data:485
msgid "Package Tracking System"
msgstr "Sistema di tracciamento dei pacchetti"

#: ../../english/intro/organization.data:487
msgid "Treasurer"
msgstr "Tesoriere"

#: ../../english/intro/organization.data:494
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Richieste d'uso del <a name=\"trademark\" href=\"m4_HOME/trademark\">marchio "
"registrato</a>"

#: ../../english/intro/organization.data:497
msgid "Salsa administrators"
msgstr "Amministratori di Salsa"

#~ msgid "Anti-harassment"
#~ msgstr "Anti-molestie"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian per bambini da 1 a 99 anni"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian per la pratica e la ricerca medica"

#~ msgid "Debian for education"
#~ msgstr "Debian nell'educazione"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian negli uffici legali"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian per disabili"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian per la scienza e la ricerca scientifica"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian per l'astronomia"

#~ msgid "Individual Packages"
#~ msgstr "Pacchetti individuali"

#~ msgid "Vendors"
#~ msgstr "Rivenditori"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Marketing Team"
#~ msgstr "Team Marketing"

#~ msgid "APT Team"
#~ msgstr "Team APT"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Il nome di ogni amministratore responsabile dei buildd puÃ² essere "
#~ "trovato su <a href=\"http://www.buildd.net\">http://www.buildd.net</a>. "
#~ "Scegliere una architetura e una distribuzione per conoscere i buildd "
#~ "disponibili e i relativi amministratori."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Gli amministratori responsabili dei buildd per una specifica architettura "
#~ "possono essere contattati all'indirizzo <genericemail arch@buildd.debian."
#~ "org>, per esempio <genericemail i386@buildd.debian.org>."

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian per le organizzazioni non-profit"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Il sistema operativo universale come Desktop"

#~ msgid "Accountant"
#~ msgstr "Ragioniere"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Team di rilascio per &ldquo;stable&rdquo;"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Custom Debian Distribution"

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordinazione della firma delle chiavi"

#~ msgid "Security Audit Project"
#~ msgstr "Progetto di verifica della sicurezza"

#~ msgid "Volatile Team"
#~ msgstr "Team Volatile"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (Non attivo: non rilasciato con squeeze)"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Amministratori Summer of Code 2013"

#~ msgid "Bits from Debian"
#~ msgstr "Notizie in pillole da Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Consiglio d'amministrazione della DebConf"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Manutentore del portachiavi Debian"

#~ msgid "Live System Team"
#~ msgstr "Team per il sistema live"

#~ msgid "Testing Security Team"
#~ msgstr "Team della sicurezza per testing"

#~ msgid "current Debian Project Leader"
#~ msgstr "l'attuale Leader del Progetto Debian"

#~ msgid "Publicity"
#~ msgstr "Pubblicità"

#~ msgid "Handhelds"
#~ msgstr "Palmari"

#~ msgid "Alioth administrators"
#~ msgstr "Amministratori di Alioth"
