#use wml::debian::template title="Usando o git para manipular o código-fonte do site web"
#use wml::debian::translation-check translation="5ec84105c40bf62785fc1159fcf33ed470a2fbc7"

<h2>Introdução</h2>

<p>O Git é um <a href="https://en.wikipedia.org/wiki/Version_control"> sistema
de controle de versão</a> que ajuda a gerenciar várias pessoas trabalhando no
mesmo material simultaneamente. Todo(a) usuário(a) pode manter uma cópia local
de um repositório principal. As cópias locais podem estar na mesma máquina ou
em todo o mundo. Usuários(as) podem então modificar a cópia local como assim
desejarem e, quando o material modificado estiver pronto, fazer
<code>commit</code> das alterações e fazer <code>push</code> para enviá-las de
volta ao repositório principal.</p>

<p>O Git não permitirá que você faça push de um commit diretamente se o
repositório remoto tiver commits (modificações) mais recentes que sua cópia
local no mesmo branch.
Nesse caso em que ocorra um conflito, por favor primeiro faça <code>fetch</code>
e <code>update</code> na sua cópia local, e faça um <code>rebase</code> de suas
novas modificações sobre o commit mais recente, conforme necessário.
</p>

<h3><a name="write-access">Acesso de escrita ao repositório Git</a></h3>

<p>
Todo o código-fonte do site Debian é gerenciado no Git, localizado em <url
https://salsa.debian.org/webmaster-team/webwml/>. Por padrão, convidados(as)
não têm permissão para fazer push dos commits para o repositório de código-fonte.
Você precisará de algum tipo de permissão para obter acesso de escrita ao
repositório.
</p>

<h4><a name="write-access-unlimited">Acesso de escrita ilimitado</a></h4>
<p>
Se você precisar de acesso ilimitado de escrita ao repositório (por exemplo,
você está prestes a se tornar um(a) contribuidor(a) frequente), considere
solicitar o acesso de escrita por meio da interface da web
<url https://salsa.debian.org/webmaster-team/webwml/> após fazer login na
plataforma Salsa do Debian.
</p>

<p>
Se você é novo(a) no desenvolvimento do site web do Debian e não tem experiência
anterior, envie também um e-mail para
<a href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>
com sua apresentação pessoal antes de solicitar acesso ilimitado de escrita.
Forneça algo útil em sua introdução, como em qual idioma ou em qual parte do
site você planeja trabalhar e quem poderia atestar por você.
</p>

<h4><a name="write-access-via-merge-request">Escreva no repositório por meio de Merge Requests</a></h4>
<p>
Se você não pretende obter acesso ilimitado de escrita ao repositório ou não
está habilitado a fazê-lo, você sempre pode enviar uma Merge Request e deixar que
outros(as) desenvolvedores(as) revisem e aceitem seu trabalho. Por favor envie
Merge Requests usando o procedimento padrão fornecido pela plataforma GitLab do
Salsa por meio de sua interface web.
</p>

<p>
As Merge Requests não são monitoradas por todos(as) os(as) desenvolvedores(as)
do site web, portanto, nem sempre elas podem ser processadas rapidamente. Se
você não tem certeza se sua contribuição será aceita, por favor envie um e-mail
para a lista de discussão
<a href="https://lists.debian.org/debian-www/">debian-www</a>
e solicite uma revisão.
</p>

<h2><a name="work-on-repository">Trabalhando no repositório</a></h2>

<h3><a name="get-local-repo-copy">Obtendo uma cópia local do repositório</a></h3>

<p>Primeiro, você precisa instalar o git para trabalhar com o repositório.
Em seguida, configure os detalhes de usuário(a) e do e-mail no seu computador
(consulte a documentação geral do git para saber como fazer isso). Em seguida,
você pode fazer um clone do repositório (em outras palavras, faça uma cópia
local) de uma de duas maneiras.</p>

<p>A maneira recomendada de trabalhar no webwml é primeiro criar uma
conta no salsa.debian.org e habilitar o acesso SSH ao git, fazendo o upload
de uma chave pública SSH na sua conta salsa. Veja as
<a href="https://salsa.debian.org/help/ssh/README.md">páginas de ajuda do
Salsa</a> para obter mais detalhes sobre como fazer isso. Em seguida, você
pode fazer o clone do repositório webwml usando o seguinte comando:</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>Se você não possui uma conta no salsa, um método alternativo é fazer o clone
do repositório usando o protocolo HTTPS:</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>Isso fornecerá a mesma cópia do repositório localmente, mas você não
poderá enviar diretamente as alterações diretamente dessa maneira.</p>

<p>A clonagem de todo o repositório webwml exigirá o download de cerca
de 500MB de dados; portanto, pode ser difícil para pessoas com conexões
de internet lentas ou instáveis. Você pode tentar a clonagem superficial
(<q>shallow clone</q>) com profundidade mínima primeiro para
um download inicial menor:</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Após obter um repositório utilizável (superficial), você pode aprofundar
a cópia superficial local e, eventualmente, convertê-la em um repositório
local completo: </p>

<pre>
  git fetch --deepen=1000 # aprofunda o repositório por mais 1000 commits
  git fetch --unshallow   # obtém todos commits em falta, converte o repositório em um completo
</pre>

<h4><a name="partial-content-checkout">Checkout parcial de conteúdo</a></h4>

<p>Você pode criar um checkout para apenas um subconjunto de páginas como
este:</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
   No webwml: crie o arquivo .git/info/sparse-checkout com um conteúdo
   como este (caso queira apenas arquivos base, traduções em inglês e português):
      /*
      !/[a-z]*/
      /english/
      /portuguese/
   Então:
   $ git checkout --
</pre>

<h3><a name="submit-changes">Enviando as alterações locais</a></h3>

<h4><a name="keep-local-repo-up-to-date">Mantendo seu repositório local atualizado</a></h4>

<p>A cada poucos dias (e definitivamente antes de começar algum trabalho de
edição!), você deve executar</p>

<pre>
   git pull
</pre>

<p>para obter quaisquer arquivos do repositório que foram alterados.</p>

<p>É altamente recomendável manter seu diretório de trabalho local do git
limpo antes de executar "git pull" e seguir o trabalho de edição. Se você
tiver alterações ainda sem commit feito, ou commits locais que não estão
presentes no repositório remoto no branch atual, fazer "git pull" criará
automaticamente "merge commits" ou até falhará devido a conflitos.
Por favor, considere manter seu trabalho inacabado em outro branch ou usar
comandos como "git stash".
</p>

<p>Nota: git é um sistema de controle de versão distribuído (ou seja, não
centralizado). Isso significa que, quando você faz commit das alterações,
elas serão armazenadas apenas no seu repositório local. Para compartilhá-las
com outras pessoas, você também precisará usar <code>push</code> para enviar
suas alterações ao repositório central do salsa.</p>

<h4><a name="example-edit-english-file">Exemplo de edição de arquivos em inglês</a></h4>

<p>
Um exemplo de como editar arquivos em inglês no repositório de código-fonte
do site é fornecido aqui. Após obter uma cópia local do repositório usando
"git clone" e antes de iniciar o trabalho de edição, execute o comando a
seguir:
</p>

<pre>
   $ git pull
</pre>

<p>Agora faça alterações nos arquivos. Quando terminar, faça o commit de
suas alterações no repositório local usando:</p>

<pre>
   $ git add caminho/para/arquivo(s)
   $ git commit -m "Sua mensagem de commit"
</pre>

<p>Se você tiver acesso ilimitado de escrita ao repositório remoto webwml,
agora poderá enviar as alterações diretamente para o repositório Salsa:</p>

<pre>
   $ git push
</pre>

<p>Se você não possui acesso direto de escrita ao repositório webwml,
considere enviar suas alterações usando a função Merge Request, conforme
fornecida pela plataforma Salsa GitLab, ou solicitando ajuda a outros(as)
desenvolvedores(as).
</p>

<p>Este é um resumo muito básico de como usar o git para manipular o
código-fonte do site Debian. Para mais informações sobre o git, leia
a documentação do git.</p>

### FIXME: Is the following still true? holgerw
### FIXME: Seems not true anymore. Needs fixup. -- byang
<h4><a name="closing-debian-bug-in-git-commits">Fechando bugs do Debian com commits git</a></h4>

<p>
Se você incluir <code>Closes: #</code><var>nnnnnn</var> na entrada de log do
commit, o bug de número <code>#</code><var>nnnnnn</var> será
fechado automaticamente quando você fizer push das suas alterações. A forma
precisa disso é a mesma que
<a href="$(DOC)/debian-policy/ch-source.html#id24">da política do Debian</a>.</p>

<h4><a name="links-using-http-https">Links usando HTTP/HTTPS</a></h4>

<p>Muitos sites web do Debian têm suporte a SSL/TLS, portanto, por favor use
links com HTTPS sempre que for possível e sensato. <strong>No entanto</strong>,
alguns sites web do Debian/DebConf/SPI/etc não têm suporte a HTTPS ou usam 
penas o CA da SPI (e não um SSL CA confiável para todos os navegadores). Para
evitar que cause exibição de mensagens de erro para não usuários(as) do Debian,
não acrescente link com HTTPS para esses sites.</p>

<p>O repositório git rejeitará commits contendo links HTTP simples para
sites web do Debian que têm suporte a HTTPS ou contendo links HTTPS para sites
web do Debian/DebConf/SPI que são conhecidos por não ter suporte a HTTPS ou
usar certificados assinados apenas pela SPI.</p>

<h3><a name="translation-work">Trabalhando em traduções</a></h3>

<p>As traduções devem sempre ser mantidas atualizadas em relação ao arquivo em
inglês correspondente. O cabeçalho "translation-check" nos arquivos de
tradução é usado para rastrear em qual versão do arquivo em inglês a
tradução atual se baseou. Se você alterar os arquivos traduzidos, precisará
atualizar o "translation-check" para corresponder ao hash do commit do git da
alteração correspondente no arquivo em inglês. Você pode encontrar esse hash
com</p>

<pre>
$ git log caminho/para/arquivo/em/inglês
</pre>

<p>Se você fizer uma nova tradução de um arquivo, use o script
<q>copypage.pl</q> e ele criará um modelo para o seu idioma,
incluindo o cabeçalho de tradução correto.</p>

<h4><a name="translation-smart-change">Alterações de tradução com smart_change.pl</a></h4>

<p><code>smart_change.pl</code> é um script desenvolvido para facilitar a
atualização de arquivos originais e suas traduções juntos. Existem duas
maneiras de usá-lo, dependendo das alterações que você está fazendo.</p>

<p>Para usar <code>smart_change</code> para atualizar os cabeçalhos de
translation-check quando você estiver trabalhando em arquivos manualmente:</p>

<ol>
  <li>Faça as alterações no(s) arquivo(s) original(is), e faça commit</li>
  <li>Atualize as traduções</li>
  <li>Execute smart_change.pl - ele vai pegar as alterações e atualizar
    os cabeçalhos nos arquivos de tradução</li>
  <li>Revise as alterações (p. ex., com "git diff")</li>
  <li>Faça commit das alterações de tradução</li>
</ol>

<p>Ou, se você estiver usando smart_change com uma expressão regular
para fazer várias alterações nos arquivos de uma só vez:</p>

<ol>
  <li>Execute <code>smart_change.pl -s s/FOO/BAR/ arq_orig1 arq_orig2 ...</code></li>
  <li>Revise as alterações (p. ex., com <code>git diff</code>)</li>
  <li>Faça commit do arquivo(s) original(is)</li>
  <li>Execute <code>smart_change.pl arq_orig1 arq_orig2</code>
    (ou seja, <strong>sem a regexp</strong> desta vez); isto vai agora
    apenas atualizar os cabeçalhos nos arquivos de tradução</li>
  <li>Finalmente, faça commit das alterações de tradução</li>
</ol>

<p>Isso é mais trabalhoso que o anterior (necessitando de dois commits), mas
inevitável devido à maneira como hashes de commit do git funcionam.</p>

<h2><a name="notifications">Obtendo notificações</a></h2>

<h3><a name="commit-notifications">Recebendo notificações de commit</a></h3>

<p>Configuramos o projeto webwml no Salsa para que os commits
sejam mostrados no canal de IRC #debian-www.</p>

<p>Se você deseja receber notificações por e-mail quando houver commit
no repositório webwml, por favor se inscreva no pseudopacote
<q>www.debian.org</q> via tracker.debian.org e ative a palavra-chave <q>vcs</q>,
seguindo estas etapas (apenas uma vez):</p>

<ul>
  <li>Abra um navegador web e acesse <url https://tracker.debian.org/pkg/www.debian.org></li>
  <li>Inscreva-se no pseudopacote <q>www.debian.org</q>. Você pode se
      autenticar usando o SSO ou registrar um e-mail e senha, caso ainda
      não estivesse usando tracker.debian.org para outros propósitos.</li>
  <li>Acesse <url https://tracker.debian.org/accounts/subscriptions/>, e em
      <q>modify keywords</q>, marque <q>vcs</q> (se ainda não estiver marcado)
      e salve.</li>
  <li>De agora em diante, você receberá e-mails quando alguém fizer commits
      no repositório webwml. Em breve adicionaremos os outros repositórios
      da equipe webmaster.</li>
</ul>

<h3><a name="merge-request-notifications">Recebendo notificações de Merge Request</a></h3>

<p>
Se você deseja receber e-mails de notificação sempre que houver novas
Merge Requests enviadas na interface web do repositório webwml na plataforma
GitLab do Salsa, você pode definir suas configurações de notificação para
o repositório webwml na interface web, seguindo estas etapas:
</p>

<ul>
  <li>Faça login na sua conta no Salsa e acesse a página do projeto;</li>
  <li>Clique no ícone do sino no topo da página inicial do projeto;</li>
  <li>Selecione o nível de notificação de sua preferência.</li>
</ul>
