<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a code injection issue in PyXDG, a library used to locate <a href="https://www.freedesktop.org/">freedesktop.org</a> configuration/cache/etc. directories.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12761">CVE-2019-12761</a>

    <p>A code injection issue was discovered in PyXDG before 0.26 via crafted Python code in a Category element of a Menu XML document in a .menu file. XDG_CONFIG_DIRS must be set up to trigger xdg.Menu.parse parsing within the directory containing this file. This is due to a lack of sanitization in xdg/Menu.py before an eval call.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.25-4+deb8u1.</p>

<p>We recommend that you upgrade your pyxdg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1819.data"
# $Id: $
