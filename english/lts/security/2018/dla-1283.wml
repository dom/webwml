<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>python-crypto generated weak ElGamal key parameters, which allowed attackers to
obtain sensitive information by reading ciphertext data (i.e., it did not have
semantic security in face of a ciphertext-only attack).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.6-4+deb7u8.</p>

<p>We recommend that you upgrade your python-crypto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1283.data"
# $Id: $
