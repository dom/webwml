<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A buffer overflow in the Squid 3's cache manager, identified by the
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4051">CVE-2016-4051</a>, was addressed by the DLA-478-1 and the Debian package
version 3.1.20-2.2+deb7u6. However, the fix was incomplete and thus a
new upload has been needed.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
3.1.20-2.2+deb7u6.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-556.data"
# $Id: $
