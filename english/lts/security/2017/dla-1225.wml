<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been discovered in Asterisk, an open source PBX and
telephony toolkit, which may result in resource exhaustion and denial of
service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17090">CVE-2017-17090</a>

    <p>memory leak from chan_skinny.
    If the chan_skinny (AKA SCCP protocol) channel driver is flooded with
    certain requests it can cause the asterisk process to use excessive
    amounts of virtual memory eventually causing asterisk to stop processing
    requests of any kind. The chan_skinny driver has been updated to release
    memory allocations in a correct manner thereby preventing any possiblity
    of exhaustion.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.8.13.1~dfsg1-3+deb7u8.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1225.data"
# $Id: $
