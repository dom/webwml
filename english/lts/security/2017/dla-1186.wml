<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the X.Org X server. An
attacker who's able to connect to an X server could cause a denial of
service or potentially the execution of arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.12.4-6+deb7u8.</p>

<p>We recommend that you upgrade your xorg-server packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1186.data"
# $Id: $
