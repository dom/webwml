<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A remote code execution vulnerability has been discovered in tomcat7.</p>

<p>When HTTP PUT was enabled (e.g., via setting the readonly initialization
parameter of the Default servlet to false) it was possible to upload a JSP
file to the server via a specially crafted request. This JSP could then be
requested and any code it contained would be executed by the server.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u16.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1166.data"
# $Id: $
