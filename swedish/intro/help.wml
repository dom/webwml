#use wml::debian::template title="Hur kan du hjälpa Debian?"
#use wml::debian::translation-check translation="e42e160ab216bcca146be3acec084557e701ceec"

<p>Om du funderar på att hjälpa till med utvecklingen av Debian
	så finns det många områden där både erfarna och oerfarna användare kan bidra:</p>

# TBD - Describe requirements per task?
# such as: knowledge of english, experience in area X, etc..

<h3>Utveckling</h3>

<ul>
	<li>Du kan paketera applikationer du har mycket erfarenhet av och anser vara
		värdefulla för Debian och bli underhållare för de paketen.  För mer
		information, läs <a href="$(HOME)/devel/">Debians utvecklarhörna</a>.</li>
		
	<li>Du kan hjälpa till med att <a href="https://security-tracker.debian.org/tracker/data/report">spåra</a>,
		<a href="$(HOME)/security/audit/">granska</a> och
		<a href="$(DOC)/manuals/developers-reference/pkg.html#bug-security">rätta</a>
		<a href="$(HOME)/security/">säkerhetsproblem</a> i paketen som ingår i
		Debian. Du kan även hjälpa till med härdning av
		<a href="https://wiki.debian.org/Hardening">paket</a>,
		<a href="https://wiki.debian.org/Hardening/RepoAndImages">förråd och avbildningar</a>
		och <a href="https://wiki.debian.org/Hardening/Goals">andra saker</a>.</li>
		
	<li>Du kan underhålla applikationer som redan finns i
		operativsystemet, särskilt de du använder mycket och känner till, genom att
		tillhandahålla rättelser (patchar) eller ytterligare information i <a
		href="https://bugs.debian.org/">felrapporteringssystemet</a> för de paketen.  Du
		kan också engagera dig direkt i paketunderhållet genom att bli medlem i ett
		underhållslag eller bli involverad i mjukvara som utvecklas
		för Debian genom att gå med i ett mjukvaruprojekt i <a
		href="https://salsa.debian.org/">Salsa</a>.</li>
	
	<li>Du kan anpassa (portera) Debian till någon arkitektur du är erfaren med
		antingen genom att påbörja en ny anpassning eller bidra till existerande anpassningar.  För mer
		information, se <a href="$(HOME)/ports/">listan över tillgängliga anpassningar</a>.</li>

	<li>Du kan hjälpa till att förbättra <a href="https://wiki.debian.org/Services">existerande</a>
	Debianrelaterade tjänster eller skapa nya tjänster som gemenskapen <a
	href="https://wiki.debian.org/Services#wishlist">behöver</a>.</li>

</ul>

<h3>Testning</h3>

<ul>
	<li>Du kan helt enkelt testa operativsystemet och programmen som följer med det
		och rapportera ännu inte kända errata eller fel som du hittar med hjälp av
		<a href="https://bugs.debian.org/">felrapporteringssystemet</a>. Försök även bläddra bland
		felen som hör till paketen du använder och ge ytterligare
		information, om du kan reproducera felen som beskrivs i dem.</li>
	<li>Du kan hjälpa till med att <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">
		testa installerar- och live-ISO-avbildningar</a>.
</ul>

<h3>Användarstöd</h3>
<ul>
	# Translators, link directly to the translated mailing lists and provide
	# a link to the IRC channel in the user's language
	<li>Om du är en erfaren användare kan du hjälpa andra användare genom
		<a href="$(HOME)/support#mail_lists">användarsändlistorna</a> eller
		på irc-kanalen <tt>#debian</tt> eller den svenska kanalen <tt>#debian.se</tt>.
		För mer information om supportmöjligheter
		och tillgängliga källor, läs <a href="$(HOME)/support">supportsidorna</a>.</li>
</ul>

<h3>Översättning</h3>
<ul>
	# TBD - link to translators mailing lists
	# Translators, link directly to your group's pages
	<li>Du kan översätta applikationer eller Debian-relaterad information
		(webbsidor, dokumentation) till ditt eget språk genom att
		engagera dig i ett översättningsprojekt (diskussioner förs normalt på
		<a href="https://lists.debian.org/debian-i18n/">i18n-sändlistan</a>).
		Du kan även
		starta en ny internationaliseringsgrupp om det inte finns någon för ditt
		språk. För mer information, läs <a
		href="$(HOME)/international/Swedish">internationaliseringssidorna</a>.</li>
	
</ul>

<h3>Dokumentation</h3>

<ul>
	<li>Du kan skriva dokumentation antingen genom att arbeta med den officiella
		dokumentationen som tillhandahålls av <a href="$(HOME)/doc/ddp">Debian Documentation
			Project</a> eller genom att bidra till <a href="https://wiki.debian.org/">Debians
			wiki</a>.</li>
 	  	 
	<li>
		Du kan tagga och kategorisera paket på webbsidan <a href="https://debtags.debian.org/">debtags</a>
		så att våra användare lättare kan hitta mjukvaran som dom söker efter.</li>
</ul>

<h3>Evenemang</h3>
<ul>
	<li>Du kan hjälpa till med utvecklingen av den <em>synliga</em> sidan av Debian och
		bidra till <a href="$(HOME)/devel/website/">webbsidan</a> eller genom att
		hjälpa till med organiserandet av <a href="$(HOME)/events/">evenemang</a>
		runt om i världen.</li>

	<li>Hjälpa till att främja Debian genom att prata om det och demonstrera det för andra.</li>
	
	<li>Hjälp till att skapa eller organisera en <a href="https://wiki.debian.org/LocalGroups">lokal
		Debiangrupp</a> med regelbundna träffar och andra aktiviteter.
	</li>

	<li>
		Du kan hjälpa till med den årliga <a href="http://debconf.org/">Debiankonferensen</a>,
		inklusive med att <a href="https://wiki.debconf.org/wiki/Videoteam">spela in video av föredrag</a>,
		<a href="https://wiki.debconf.org/wiki/FrontDesk">möta deltagare när dom ankommer</a>,
		<a href="https://wiki.debconf.org/wiki/Talkmeister">hjälpa föredragshållare innan föredrag</a>,
		speciella evenemang (som ost och vin-festen), förberedelse, nedpackning och andra saker.
	</li>
	
	<li>
		Du kan hjälpa till att organisera den årliga <a href="http://debconf.org/">Debiankonferensen</a>,
		mini-DebConfs för ditt område,
		<a href="https://wiki.debian.org/DebianDay">Debian Day-fester</a>,
		<a href="https://wiki.debian.org/ReleaseParty">utgivningsfester</a>,
		<a href="https://wiki.debian.org/BSP">felrättningsfester</a>,
		<a href="https://wiki.debian.org/Sprints">utvecklingssprints</a> och
		<a href="https://wiki.debian.org/DebianEvents">andra evenemang</a> världen runt.
	</li>

</ul>
<h3>Donera</h3>
<ul>

	<li>Du kan <a href="$(HOME)/donations">donera pengar, utrustning och tjänster</a> till Debian-projektet så att
		antingen dess användare eller utvecklare kan dra nytta av dem. Vi letar ständigt
		efter <a href="$(HOME)/mirror/">speglar runt om i världen</a> som våra användare kan lita på och
		<a href="$(HOME)/devel/buildd/">automatkompileringsnätverk</a> för våra anpassare.</li>
</ul>
<h3>Använd Debian!</h3>
<ul>
	<li>
		Du kan <a href="https://wiki.debian.org/ScreenShots">göra skärmdumpar</a> av
		paket och <a href="https://screenshots.debian.net/upload">ladda upp</a> dessa till
		<a href="https://screenshots.debian.net/">screenshots.debian.net</a> så att
		våra användare kan se hur mjukvara i Debian ser ut innan dom använder den.</li>

	<li>Du kan aktivera <a href="https://packages.debian.org/popularity-contest">\
	rapportering till popularity-contest</a> så vi kan veta vilka paket som är
	användbara för alla.</li>

</ul>

<p>Som du kan se, finns det många sätt att engagera sig i projektet
	och bara ett fåtal av dem kräver att du blir Debianutvecklare.  Många av de
	olika projekten har mekanismer som tillåter direkt tillgång till källkodsträd
	till medhjälpare som har visat att de är pålitliga och värdefulla.  Oftast
	kommer de som finner att de kan bli mycket mer engagerade i Debian vilja <a
	href="$(HOME)/devel/join">gå med i projektet</a>, men det är inte
	alltid ett krav.</p>

<h3>Organisationer</h3>

<p>
Din utbildnings-, kommersiella, ideella eller statliga organisation
kan var intresserad av att hjälpa Debian med hjälp av dina resurser.
Din organisation kan
<a href="https://www.debian.org/donations">donera till oss</a>,
<a href="https://www.debian.org/partners/">bilda löpande partnerskap med oss</a>,
<a href="https://www.debconf.org/sponsors/">sponsra våran konferens</a>,
<a href="https://wiki.debian.org/MemberBenefits">tillhandahålla gratis produkter eller tjänster till bidragslämnare till Debians</a>,
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">tillhandahålla gratis värdskap för experiment med Debiantjänster</a>,
upprätthålla speglingar av vår
<a href="https://www.debian.org/mirror/ftpmirror">mjukvara</a>,
<a href="https://www.debian.org/CD/mirroring/">installationsmedia</a>
or <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">konferensvideos</a>
eller promota vår mjukvara och vår gemenskap genom att
<a href="https://www.debian.org/users/">ge en rekommendation</a>
eller sälja Debian <a href="https://www.debian.org/events/merchandise">merchandise</a>,
<a href="https://www.debian.org/CD/vendors/">installationsmedia</a>,
<a href="https://www.debian.org/distrib/pre-installed">för-installerade system</a>,
<a href="https://www.debian.org/consultants/">konsultande</a> eller
<a href="https://wiki.debian.org/DebianHosting">hosting</a>.
</p>

<p>
Du kan även hjälpa till att uppmuntra din personal att delta i vår gemenskap genom
att exponera dem för Debian genom att använda vårt operativsystem i din organisation,
lära dom om operativsystemet Debian samt gemenskapen,
leda dem att bidra under deras arbetstid eller
genom att skicka dom till våra <a href="$(HOME)/events/">evenemang</a>.
</p>

# <p>Related links:
